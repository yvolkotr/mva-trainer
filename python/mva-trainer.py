#!/usr/bin/env python3
"""TODO add module description

"""
import argparse
import gc
import os
from multiprocessing import Pool

import HelperModules.helperfunctions as hf
from HelperModules.configparser import configparser
from HelperModules.conversionhandler import conversionhandler
from HelperModules.injectionhandler import injectionhandler
from HelperModules.messagehandler import ErrorMessage, InfoMessage, InfoDelimiter
from HelperModules.modeloptimisation import modeloptimisation
from ModelModules.modelhandler import modelhandler
from PlotterModules.plothandler import plothandler


def inject(c, a, fname, processid, maxprocessid):
    """Multi-processing function to do an injection.

    :param c: configparser object
    :param a: argparse object
    :param fname: file name
    :param processid: id of the process
    :param maxprocessid: maximum id of all processes
    :returns:

    """
    inj = injectionhandler(c, a)
    inj.inject(fname, processid, maxprocessid)
    del inj
    gc.collect()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    common_arguments = parser.add_argument_group(title="\033[1mCommon arguments\033[0m")
    injection_arguments = parser.add_argument_group(title="\033[1mInjection-specific arguments\033[0m")
    other_arguments = parser.add_argument_group(title="\033[1mOther arguments\033[0m")
    # Common arguments
    common_arguments.add_argument("-c", "--configfile", help="Config file", required=True)
    common_arguments.add_argument("--convert",
                        help="flag to set in case you want to run the conversion",
                        action='store_true',
                        dest="convert",
                        default=False)
    common_arguments.add_argument("--train",
                        help="flag to set in case you want to run the training",
                        action='store_true',
                        dest="train",
                        default=False)
    common_arguments.add_argument("--evaluate",
                        help="flag to set in case you want to run the evaluation",
                        action='store_true',
                        dest="evaluate",
                        default=False)
    common_arguments.add_argument("--inject",
                        help="flag to set in case you want to run the injection",
                        action='store_true',
                        dest="inject",
                        default=False)
    common_arguments.add_argument("--optimise",
                        help="flag to set in case you want to run the optimisation",
                        action='store_true',
                        dest="optimise",
                        default=False)
    # Injection-specific arguments
    injection_arguments.add_argument("-i", "--input_path", help="Ntuple path")
    injection_arguments.add_argument("-o", "--output_path", help="output path for the converted ntuples")
    injection_arguments.add_argument("-f",
                        "--filter",
                        nargs="+",
                        dest="wildcard",
                        help="additional string to filter input files",
                        default=[''])
    injection_arguments.add_argument("--ignore",
                        nargs="+",
                        dest="ignore",
                        help="additional string to ignore input files",
                        default=[''])
    injection_arguments.add_argument("-t", "--treefilter", dest="treewildcard", help="additional string to filter trees", type=str, default='')
    injection_arguments.add_argument("--processes",
                        help="the number of parallel processes to run",
                        type=int,
                        default=8)
    injection_arguments.add_argument("--additionalconfigs", help="Additional config files for injection", nargs="+",)
    injection_arguments.add_argument("--optimisationpath",
                        dest="optimisationpath",
                        help="Path to directory to store optimisation configs.")
    injection_arguments.add_argument("--nModels",
                        dest="nModels",
                        help="Number of configs to be created for hyperparameter optimisation.",
                        type=int,
                        default=10)
    injection_arguments.add_argument("--HO_options",
    			help="Hyperparameter running option",
    			choices=[
    			        "Converter",
    			        "Trainer",
    			        "Evaluater",
    			        "All"],
    			dest="HO_options",
    			default="Trainer")
    # Other arguments
    other_arguments.add_argument("--plots",
                        nargs="+",
                        help="the dedicated plots to be produced, default is 'all'",
                        default=["all"],
                        choices=[
                            "all",
                            "TrainStats",
                            "DataMC",
                            "DataMCscaled",
                            "nonNegativeWeights",
                            "NegativeWeightSummary",
                            "TrainTest",
                            "CorrelationMatrix",
                            "CorrelationComparison",
                            "Separation2D"])
    other_arguments.add_argument("--plots-only",
                        help="flag to set in case you only want to redo the plots",
                        action='store_true',
                        dest="plots_only",
                        default=False)
    other_arguments.add_argument("--data-only",
                        help="flag to set in case you only want to convert the data",
                        action='store_true',
                        dest="data_only",
                        default=False)
    args = parser.parse_args()

    cfgset = configparser(args.configfile)
    cfgset.read()
    if args.convert:
        cfgset.Print()
        convhandler = conversionhandler(cfgset)
        if not args.plots_only:
            convhandler.convert()
        if not args.data_only:
            phandler = plothandler(cfgset)
            phandler.doplots(args)
    if args.train:
        cfgset.Print()
        mhandler = modelhandler(cfgset)
        mhandler.runTraining()
    if args.evaluate:
        cfgset.Print()
        phandler = plothandler(cfgset)
        phandler.doevalplots(args)
    if args.inject:
        if args.input_path is None:
            ErrorMessage("No input path given!")
        if args.output_path is None:
            ErrorMessage("No output path given!")
        for dirpath, dirnames, filenames in os.walk(args.input_path):
            structure = os.path.join(args.output_path, dirpath[len(args.input_path):].strip("/"))
            if not os.path.isdir(structure):
                os.makedirs(structure)
        InfoMessage("Reading configs...")
        if args.additionalconfigs is not None:
            addconfigs = [configparser(cfg) for cfg in args.additionalconfigs]
        else:
            addconfigs = []
        cfgset.Print()
        InfoDelimiter(3)
        for addconfig in addconfigs:
            addconfig.read()
            addconfig.Print()
            InfoDelimiter(3)
        cfgsets = [cfgset] + addconfigs
        fnames = hf.absfpaths(args.input_path)
        fprocess = []
        fnames = [f for f in fnames if ".root" in f]
        fnames = [f for f in fnames if (any(w in f for w in args.wildcard) or args.wildcard == [''])]
        fnames = [f for f in fnames if (all(i not in f for i in args.ignore) or args.ignore == [''])]
        p = Pool(args.processes, maxtasksperchild=1)
        MAX_ID = len(fnames)
        results = [p.apply_async(inject, (cfgsets, args, f, i + 1, MAX_ID,)) for i, f in enumerate(fnames)]
        results_g = [r.get() for r in results]
        p.close()
        p.join()
    if args.optimise:
        modeloptimiser = modeloptimisation(args, cfgset)
        modeloptimiser.writeconfigs()
