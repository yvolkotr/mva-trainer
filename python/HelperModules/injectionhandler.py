"""Module to handle the injection of predictions into ntuples

"""

import collections
import os
from array import array

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd
import ROOT
import uproot
import awkward as ak
from HelperModules.messagehandler import (ErrorMessage, WarningMessage, InfoDoneMessage,
                                          InfoMessage)
from HelperModules.datascaler import scaler
from ModelModules.modelhandler import modelhandler


def loaddata(fpath, tname, inputs):
    """Function to load event data using uproot.

    :param fpath: String representing the file path
    :param tname: Name of the TTree to read from
    :param inputs: List of input variables to read
    :returns: Pandas DataFrame

    """

    try:
        evts = uproot.open(f"{fpath}:{tname}")
    except FileNotFoundError as e:
        ErrorMessage(f"Could not open {fpath}. You need to check this! {e}")
    try:
        e = ak.to_dataframe(evts.arrays(inputs, library="ak"))
        for branch in inputs:
            if any(e[branch].isna()):
                WarningMessage(f"Branch {branch} from {tname} tree in file {fpath} has NaN/null value(s). Setting them to -1.")
                e[branch] = e[branch].fillna(-1) #replace NaN values with -1
    except uproot.exceptions.KeyInFileError as e:
        ErrorMessage(f"The variable {e.args[0]} is missing in tree {tname} in file {fpath}. Consider using the 'IgnoreTreenames' option.")
    return e


def splitx(xin, cfgset):
    """Function to split the read dataset into chunks based on the FoldSplitVariable

    :param xin: Input dataframe to be split
    :param cfgset: config parameters stored in a configparser object
    :returns: List of Pandas DataFrames

    """
    nfolds = cfgset.get("GENERAL").get("Folds")
    splitvar = cfgset.get("GENERAL").get("FoldSplitVariable")
    vnames = [v.get("Name") for v in cfgset.get("VARIABLE")]
    return [xin[xin[splitvar] % nfolds == i][vnames] for i in range(nfolds)]

class injectionhandler():
    """Class taking care of the injection of predictions into ntuples

    """

    def __init__(self, cfgsets, args):
        self.m_cfgsets = cfgsets
        self.m_args = args
        self.m_mhandlers = [modelhandler(cfgset) for cfgset in self.m_cfgsets]
        self.m_models = [s.get("MODEL") for s in self.m_cfgsets]
        self.checkmodelnames()

    def checkmodelnames(self):
        """Method to perform a a check whether all used model names are unique.
        This is relevant as the model's name will be used in the TTree.

        :returns:

        """
        mnames = [m.get("Name") for m in [cfgset.get("MODEL") for cfgset in self.m_cfgsets]]
        if len(mnames) != len(set(mnames)):
            dpl = [item for item, count in collections.Counter(mnames).items() if count > 1]
            ErrorMessage(f"You are using models that have identical `Model Names`. The following names occur multiple times {dpl}.")

    def processtree(self, infile, outfile, tname, fname):
        """Wrapper Method to process a single TTree

        :param infile: Input root file
        :param outfile: Output root file
        :param tname: Name of the TTree to process
        :param fname: Name of the File to process
        :returns:

        """
        Tree = infile.Get(tname)
        if tname not in self.m_args.treewildcard and self.m_args.treewildcard != "":
            Tree.Delete()
            return
        # black listing a few trees which are simply copied
        if any(any(t in tname for t in c.get("GENERAL").get("IgnoreTreenames")) for c in self.m_cfgsets):
            NewTree = Tree.CloneTree(-1, "fast")
            outfile.Write()
            Tree.Delete()
            return
        NewTree = Tree.CloneTree(0)
        NewBranches = {}
        all_labels = []
        for c in self.m_cfgsets:
            all_labels.append([out.get("Label") for out in c.get("OUTPUT")])
            for l in [out.get("Label").replace(" ", "_") for out in c.get("OUTPUT")]:
                NewBranches[f"{l}"] = array('f', [0])
        if len(np.unique(all_labels)) < len(all_labels):
            ErrorMessage("Some of your output labels are not unique across the used config files!")
        for BranchName, NewBranch in NewBranches.items():
            NewTree.Branch(BranchName, NewBranch, BranchName + "/F")
        if Tree.GetEntries() == 0:
            outfile.Write()
            Tree.Delete()
            return
        event_dict = {}
        for entry_number, event_dict in zip(range(0, Tree.GetEntries()), self.getpdicts(fname, tname)):
            Tree.GetEntry(entry_number)
            for name in NewBranches:
                NewBranches[name][0] = event_dict[name]
            NewTree.Fill()
        event_dict.clear()
        NewBranches.clear()
        Tree.Delete()
        outfile.Write()

    def getx(self, fpath, tname, cfgset):
        """Getter method to retrieve input data

        :param fpath: File path
        :param tname: Name of TTree object
        :param cfgset: Configparser object containing config settings
        :returns: Input data in Pandas DataFrame format

        """
        inputs = [v.get("Name") for v in cfgset.get("VARIABLE")] + [cfgset.get("GENERAL").get("FoldSplitVariable")]
        xin = loaddata(fpath=fpath, tname=tname, inputs=inputs)
        return xin

    def getsortindece(self, xin, cfgset):
        """Method to retrieve the indeces of the original ordering of the dataset

        :param xin: Input dataset in Pandas DataFrame format
        :param cfgset: Configparser object containing config settings
        :returns: Numpy array containing indeces, reflecting the original order of the dataset

        """
        nfolds = cfgset.get("GENERAL").get("Folds")
        splitvar = cfgset.get("GENERAL").get("FoldSplitVariable")
        indece = [[] for i in range(nfolds)]
        for ind, val in enumerate(xin[splitvar].values):
            indece[int(val % nfolds)].append(ind)
        return np.concatenate(indece).astype(int)

    def sortp(self, pin, indece):
        """Function to perform a sort of the predictions back to the original ordering.

        :param pin: Input predictions
        :param indece: Indeces of the individual predictions
        :returns: Pandas DataFrame with the sorted predictions

        """
        sort_index = [i[0] for i in sorted(enumerate(indece), key=lambda x:x[1])]
        pout = pin.values[sort_index]
        pnames = [name.replace(" ", "_") for name in pin.keys()]
        return pd.DataFrame(pout, columns=pnames)

    def getpdicts(self, fpath, tname):
        """Function performing the pre-calculation, i.e. calculating

        :param fpath: file path
        :param tname: treename
        :param mhandler: modelhandler object
        :param cfgset: configparser object
        :returns: (list of dictionaries): List of dicts representing the model(s) predictions.

        """
        pdfs = []
        for mhandler, cfgset in zip(self.m_mhandlers, self.m_cfgsets):
            sc = scaler(cfgset)
            sc.load(f"{hf.addslash(cfgset.get('DIR').get('Model'))}scaler.json")
            xin = self.getx(fpath, tname, cfgset)
            indece = self.getsortindece(xin, cfgset)
            xins = splitx(xin, cfgset)
            xins = [sc.transform(xin=xin) for xin in xins if len(xin)>0]
            ptmp = [mhandler.predict(xin, fold)
                    for xin, fold in zip(xins, range(cfgset.get("GENERAL").get("Folds"))) if len(xin) > 0]
            ptmp = pd.concat(ptmp)
            pfinal = self.sortp(ptmp, indece)
            pdfs.append(pfinal)
        p = pd.concat(pdfs, axis=1)
        return p.to_dict('records')

    def inject(self, fname, fid, maxfid):
        """Function to perform the actual injection of predictions into the ntuples.

        :param fname: Input file name
        :param fid: Input file id (to do a count)
        :param maxfid: Largest file id
        :returns:

        """
        InfoMessage(f"Processing {fid}/{maxfid}:\t{fname}")
        # Create a new root file
        infile = ROOT.TFile(fname)
        outfile = ROOT.TFile(fname.replace(os.path.abspath(self.m_args.input_path), os.path.abspath(self.m_args.output_path)), "recreate")
        # Loop over trees in root file
        keys = infile.GetListOfKeys()
        tnames = []
        for key in keys:
            if key.GetClassName() != "TTree":  # ignore TObjects that are not TTrees
                continue
            if key.ReadObj().GetName() == "":  # ignore TTrees that do not have a name
                continue
            tnames.append(key.ReadObj().GetName())
        if len(tnames) != len(np.unique(tnames)):
            WarningMessage(f"Found trees with identical names in {fname}! You need to check this!")
            tnames = np.unique(tnames)
        for tname in tnames:
            self.processtree(infile=infile,
                             outfile=outfile,
                             tname=tname,
                             fname=fname)
        del tnames
        outfile.Close()
        outfile.Delete()
        infile.Close()
        infile.Delete()
        InfoDoneMessage(f"Finished {fname}")
