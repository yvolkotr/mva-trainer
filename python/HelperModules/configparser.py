"""Module handling parsing of config files

"""
import numpy as np
from HelperModules.directoryhandler import directoryhandler
from HelperModules.messagehandler import ErrorMessage
from HelperModules.optionhandler import optionhandler


class configparser():
    """Class to handle the parsing of a config file.

    """

    def __init__(self, fname):
        self.m_fname = fname
        self.m_blocks = None
        self.m_lnumbers = []
        self.m_cfgvals = {}
        self.m_mname = None
        self.m_dirhandler = None

    def _fill(self, oblocks, opthandler, lnumbers):
        """Reading the individual input blocks and filling optionhandler objects

        :param oblock: Block of input strings representing an option block in the config
        :returns: optionhandler object representing the aforementioned option block

        """
        for item, lnumber in zip(oblocks, lnumbers):
            opthandler.set_option(item, lnumber)
        return opthandler

    def _initBlock(self, block):
        d = {"GENERAL": self._initGeneral(),
             "VARIABLE": self._initVariable(),
             "OUTPUT": self._initOutput(),
             "SAMPLE": self._initSample(),
             "GRAPHNODE": self._initGraphNode(),
             "DNNMODEL": self._initDNNModel(),
             "BDTMODEL": self._initBDTModel(),
             "GNNMODEL": self._initGNNModel()}
        return d[block]

    def _initGeneral(self):
        """Initialise method for GENERAL block

        :returns:

        """
        opthandler = optionhandler()
        opthandler.setdescr("GENERAL")
        opthandler.register("Job", None)
        opthandler.register("NtuplePath", None)
        opthandler.register("DoNOnly", None)
        opthandler.register("Selection", "")
        opthandler.register("MCWeight", "")
        opthandler.register("Treename", None)
        opthandler.register("IgnoreTreenames", [], islist=True)
        opthandler.register("InputScaling", None)
        opthandler.register("RecalcScaler", True)
        opthandler.register("OutputScaling", None)
        opthandler.register("WeightScaling", True)
        opthandler.register("WeightsToUnity", True)
        opthandler.register("TreatNegWeights", "Scale")
        opthandler.register("Folds", 2)
        opthandler.register("FoldSplitVariable", "eventNumber")
        opthandler.register("ATLASLabel", "")
        opthandler.register("CMLabel", "")
        opthandler.register("CustomLabel", "")
        opthandler.register("DoCtrlPlots", True)
        opthandler.register("DoYields", False)
        opthandler.register("Blinding", None)
        opthandler.register("SBBlinding", None)
        opthandler.register("SignificanceBlinding", None)
        opthandler.register("UnderFlowBin", True)
        opthandler.register("OverFlowBin", True)
        opthandler.register("PlotFormat", ["pdf", "png", "eps"], islist=True)
        opthandler.register("LossPlotXScale", "Log")
        opthandler.register("LossPlotYScale", "Linear")
        opthandler.register("StackPlotScale", "Linear")
        opthandler.register("StackPlot2DScale", "Linear")
        opthandler.register("SeparationPlotScale", "Linear")
        opthandler.register("ConfusionPlotScale", "Linear")
        opthandler.register("ConfusionMatrixNorm", "row")
        opthandler.register("PermImportancePlotScale", "Linear")
        opthandler.register("PermImportanceShuffles", 5)
        opthandler.register("LargeCorrelation", 0.5)
        opthandler.register("RatioMax", 1.55)
        opthandler.register("RatioMin", 0.45)
        opthandler.register("ROCSampling", 100)
        opthandler.register("MinSBSignalYield", 0.3)
        opthandler.register("MinSsqrtBSignalYield", 0.3)
        opthandler.register("NbinsSB2DPlots", 100)
        opthandler.register("SoverBSF", 1)
        opthandler.register("SoversqrtBSF", 1)
        opthandler.register("RandomSeed", None)
        opthandler.register("ComparisonTest", "Anderson-Darling")

        opthandler.register_check("Job", check="set")
        opthandler.register_check("NtuplePath", check="set")
        opthandler.register_check("Treename", check="set")
        opthandler.register_check("InputScaling", check="set")
        opthandler.register_check("InputScaling", check="inlist", lst=["None", "minmax", "minmax_symmetric", "standard"])
        opthandler.register_check("TreatNegWeights", check="inlist", lst=["None", "Scale", "SetZero"])
        opthandler.register_check("Folds", check="larger", thr=1)
        opthandler.register_check("PlotFormat", check="is_subset", lst=["pdf", "png", "eps", "jpeg", "gif", "svg"])
        opthandler.register_check("LossPlotXScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("LossPlotYScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("StackPlotScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("StackPlot2DScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("SeparationPlotScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("ConfusionPlotScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("ConfusionMatrixNorm", check="is_inlist", lst=["row", "column", "None"])
        opthandler.register_check("PermImportancePlotScale", check="is_inlist", lst=["Log", "Linear"])
        opthandler.register_check("PermImportanceShuffles", check="larger", thr=1)
        opthandler.register_check("LargeCorrelation", check="larger", thr=0)
        opthandler.register_check("LargeCorrelation", check="smaller", thr=1)
        opthandler.register_check("RatioMin", check="larger", thr=0)
        opthandler.register_check("RatioMax", check="larger", thr=0)
        opthandler.register_check("RatioMax", check="larger", thr="RatioMin")
        opthandler.register_check("ROCSampling", check="larger", thr=0)
        opthandler.register_check("MinSBSignalYield", check="larger", thr=0)
        opthandler.register_check("MinSBSignalYield", check="smaller", thr=1)
        opthandler.register_check("MinSsqrtBSignalYield", check="larger", thr=0)
        opthandler.register_check("MinSsqrtBSignalYield", check="smaller", thr=1)
        opthandler.register_check("NbinsSB2DPlots", check="larger", thr=0)
        opthandler.register_check("SoverBSF", check="larger", thr=0)
        opthandler.register_check("SoversqrtBSF", check="larger", thr=0)
        opthandler.register_check("SBBlinding", check="larger", thr=0)
        opthandler.register_check("SignificanceBlinding", check="larger", thr=0)
        opthandler.register_check("ComparisonTest", check="in_list", lst=["Kolmogorov-Smirnov", "Anderson-Darling"])
        return opthandler

    def _initVariable(self):
        """Initialise method for VARIABLE block

        :returns:

        """
        opthandler = optionhandler()
        opthandler.setdescr("VARIABLE")
        opthandler.register("Name", None)
        opthandler.register("Label", None)
        opthandler.register("Binning", None, islist=True)
        opthandler.register("ScalingRange", None, islist=True)
        opthandler.register("CustomBinning", None, islist=True)
        opthandler.register("Ymin", 0.0)
        opthandler.register("Ymax", None)
        opthandler.register("Scaling", None)
        opthandler.register("Type", None)
        opthandler.register("Scale", "Linear")

        opthandler.register_check("Name", check="set")
        opthandler.register_check("Label", check="set")
        opthandler.register_check("Scaling", check="inlist", lst=["None", "minmax", "minmax_symmetric", "standard", "pT_scale"])
        opthandler.register_check(["Binning", "CustomBinning"], check="set")
        opthandler.register_check("Scale", check="is_inlist", lst=["Linear", "Log"])
        return opthandler

    def _initOutput(self):
        """Initialise method for OUTPUT block

        :returns:

        """
        opthandler = optionhandler()
        opthandler.setdescr("Output")
        opthandler.register("Name", None)
        opthandler.register("Label", None)
        opthandler.register("Binning", None, islist=True)
        opthandler.register("CustomBinning", None, islist=True)
        opthandler.register("Ymin", 0.0)
        opthandler.register("Ymax", None)
        opthandler.register("Scale", "Linear")

        opthandler.register_check("Name", check="set")
        opthandler.register_check("Label", check="set")
        opthandler.register_check(["Binning", "CustomBinning"], check="set")
        opthandler.register_check("Scale", check="is_inlist", lst=["Linear", "Log"])
        return opthandler

    def _initSample(self):
        """Initialise method for SAMPLE block

        :returns:

        """
        opthandler = optionhandler()
        opthandler.setdescr("SAMPLE")
        opthandler.register("Name", None)
        opthandler.register("Type", None)
        opthandler.register("NtupleFiles", None, islist=True)
        opthandler.register("Treename", None)
        opthandler.register("Selection", "")
        opthandler.register("MCWeight", "")
        opthandler.register("Target", np.nan)
        opthandler.register("PenaltyFactor", 1)
        opthandler.register("FillColor", None)
        opthandler.register("ScaleToBkg", False)

        opthandler.register_check("Name", check="set")
        opthandler.register_check("Type", check="set")
        opthandler.register_check("Type", check="inlist", lst=["Background", "Signal", "Signal_Scaled", "Fake", "Systematic", "Data"])
        opthandler.register_check("Target", check="set", except_case="Data")
        opthandler.register_check("FillColor", check="set", except_case="Data")
        opthandler.register_check("NtupleFiles", check="set")
        opthandler.register_check("PenaltyFactor", check="larger", thr=0)
        return opthandler

    def _initGraphNode(self):
        """Initialise method for GRAPHNODE block

        :returns:

        """
        opthandler = optionhandler()
        opthandler.setdescr("GRAPHNODE")
        opthandler.register("Name", None)
        opthandler.register("Type", None)
        opthandler.register("Features", None, islist=True)
        opthandler.register("Targets", None, islist=True)
        opthandler.register("EdgeFeatures", None, isnlist=True)
        opthandler.register("PruneIfValue", None, islist=True)

        opthandler.register_check("Name", check="set")
        opthandler.register_check("Type", check="set")
        return opthandler

    def _initBasicModel(self):
        """Initialise method for a basic model (used later for all models)

        :returns:

        """
        opthandler = optionhandler()
        opthandler.register("Name", None)
        opthandler.register("Type", None)
        opthandler.register("LearningRate", 0.001)
        opthandler.register("StepLearningRate", 0.0001)
        opthandler.register("MaxLearningRate", 0.1)
        opthandler.register("MinLearningRate", 0.0001)
        opthandler.register("ValidationSize", 0.2)
        opthandler.register("Metrics", None, islist=True)
        opthandler.register("SampleFrac", 1)
        opthandler.register("Verbosity", 1)
        opthandler.register("GetONNXModel", True)
        opthandler.register("ONNXVersion", 10)
        opthandler.register("DropoutIndece", [-1], islist=True)
        opthandler.register("DropoutProb", 0.1)
        opthandler.register("BatchNormIndece", [-1], islist=True)

        opthandler.register_check("Name", check="set")
        opthandler.register_check("Type", check="set")
        opthandler.register_check("Type", check="inlist", lst=['Classification-DNN',
                                                               'Classification-BDT',
                                                               'Classification-WGAN',
                                                               'Classification-ACGAN',
                                                               'Classification-GNN',
                                                               'Regression-DNN',
                                                               'Regression-GNN',
                                                               'Regression-BDT',
                                                               '3LZ-Reconstruction',
                                                               '4LZ-Reconstruction'])
        opthandler.register_check("LearningRate", check="larger", thr=0)
        opthandler.register_check("DropoutProb", check="larger", thr=0)
        opthandler.register_check("StepLearningRate", check="larger", thr=0.00001)
        opthandler.register_check("MaxLearningRate", check="larger", thr=0)
        opthandler.register_check("MinLearningRate", check="larger", thr=0)
        opthandler.register_check("MaxLearningRate", check="larger", thr="MinLearningRate")
        opthandler.register_check("ValidationSize", check="larger", thr=0)
        opthandler.register_check("ValidationSize", check="smaller", thr=1)
        opthandler.register_check("Verbosity", check="inlist", lst=[0, 1, 2])
        opthandler.register_check("GetONNXModel", check="inlist", lst=[True, False])
        return opthandler

    def _initDNNModel(self):
        """Initialise method for DNNMODEL block

        :returns:

        """
        opthandler = self._initBasicModel()
        opthandler.setdescr("DNNMODEL")
        opthandler.register("Nodes", None, islist=True)
        opthandler.register("Epochs", 100)
        opthandler.register("BatchSize", 32)
        opthandler.register("MaxNodes", 100)
        opthandler.register("MinNodes", 10)
        opthandler.register("StepNodes", 10)
        opthandler.register("MaxLayers", 7)
        opthandler.register("MinLayers", 1)
        opthandler.register("Patience", None)
        opthandler.register("MinDelta", None)
        opthandler.register("SetActivationFunctions", ["relu", "sigmoid", "tanh", "elu"], islist=True)
        opthandler.register("ActivationFunctions", None, islist=True)
        opthandler.register("OutputActivation", None)
        opthandler.register("Loss", None)
        opthandler.register("Optimiser", "Nadam")
        opthandler.register("HuberDelta", 1.0)
        opthandler.register("RestoreBest", True)
        opthandler.register("ReweightTargetDistribution", False)

        opthandler.register_check("Nodes", check="set")
        opthandler.register_check("Nodes", check="equal_len", l="ActivationFunctions")
        opthandler.register_check("Epochs", check="larger", thr=0)
        opthandler.register_check("BatchSize", check="larger", thr=0)
        opthandler.register_check("MinNodes", check="larger", thr=0)
        opthandler.register_check("MaxNodes", check="larger", thr=0)
        opthandler.register_check("MaxNodes", check="larger", thr="MinNodes")
        opthandler.register_check("MinLayers", check="larger", thr=0)
        opthandler.register_check("MaxLayers", check="larger", thr=0)
        opthandler.register_check("MaxLayers", check="larger", thr="MinLayers")
        opthandler.register_check("Patience", check="larger", thr=0)
        opthandler.register_check("MinDelta", check="larger", thr=0)
        opthandler.register_check("ActivationFunctions", check="set")
        opthandler.register_check("OutputActivation", check="set")
        opthandler.register_check("Loss", check="inlist", lst=[
            "binary_crossentropy",
            "categorical_crossentropy",
            "MSE",
            "MAE",
            "HuberLoss"])
        opthandler.register_check("RestoreBest", check="inlist", lst=[True, False])
        opthandler.register_check("ReweightTargetDistribution", check="inlist", lst=[True, False])
        return opthandler

    def _initGNNModel(self):
        """Initialise method for GNNMODEL block

        :returns:

        """
        opthandler = self._initBasicModel()
        opthandler.setdescr("GNNMODEL")
        opthandler.register("GraphStructure", "homogeneous")
        opthandler.register("GraphLayers", None, islist=True)
        opthandler.register("GraphChannels", None, islist=True)
        opthandler.register("GlobalNodes", None, islist=True)
        opthandler.register("FinalNodes", None, islist=True)
        opthandler.register("MaxNodes", 100)
        opthandler.register("MinNodes", 10)
        opthandler.register("StepNodes", 5)
        opthandler.register("MaxLayers", 7)
        opthandler.register("MinLayers", 1)
        opthandler.register("GraphActivationFunctions", None, islist=True)
        opthandler.register("GlobalActivationFunctions", None, islist=True)
        opthandler.register("FinalActivationFunctions", None, islist=True)
        opthandler.register("SetActivationFunctions", ["relu", "sigmoid", "tanh", "elu"], islist=True)
        opthandler.register("SetGraphLayers", ["SAGEConv", "GATConv"], islist=True)
        opthandler.register("OutputActivation", None)
        opthandler.register("Epochs", 100)
        opthandler.register("BatchSize", 32)
        opthandler.register("LearningRate", 0.001)
        opthandler.register("ValidationSize", 0.25)
        opthandler.register("Optimiser", "Nadam")
        opthandler.register("Loss", None)
        opthandler.register("Patience", None)
        opthandler.register("MinDelta", None)
        opthandler.register("RestoreBest", True)

        opthandler.register_check("GraphStructure", check="set")
        opthandler.register_check("GraphStructure", check="inlist", lst=["homogeneous", "heterogeneous"])
        opthandler.register_check("GraphLayers", check="set")
        opthandler.register_check("GraphLayers", check="is_subset", lst=["GCNConv", "GATConv", "SAGEConv"])
        opthandler.register_check("GraphLayers", check="equal_len", l="GraphActivationFunctions")
        opthandler.register_check("GraphChannels", check="equal_len", l="GraphActivationFunctions")
        opthandler.register_check("GlobalNodes", check="set")
        opthandler.register_check("GlobalNodes", check="equal_len", l="GlobalActivationFunctions")
        opthandler.register_check("FinalNodes", check="set")
        opthandler.register_check("FinalNodes", check="equal_len", l="FinalActivationFunctions")
        opthandler.register_check("MinNodes", check="larger", thr=0)
        opthandler.register_check("MaxNodes", check="larger", thr="MinNodes")
        opthandler.register_check("MinLayers", check="larger", thr=0)
        opthandler.register_check("MaxLayers", check="larger", thr="MinLayers")
        opthandler.register_check("GraphActivationFunctions", check="set")
        opthandler.register_check("GraphActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "tanh", "elu", "leaky_relu"])
        opthandler.register_check("GlobalActivationFunctions", check="set")
        opthandler.register_check("GlobalActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "tanh", "elu", "leaky_relu"])
        opthandler.register_check("FinalActivationFunctions", check="set")
        opthandler.register_check("FinalActivationFunctions", check="is_subset", lst=["relu", "sigmoid", "tanh", "elu", "leaky_relu"])
        opthandler.register_check("OutputActivation", check="set")
        opthandler.register_check("OutputActivation", check="inlist", lst=["relu", "sigmoid", "tanh", "elu", "leaky_relu"])
        opthandler.register_check("BatchSize", check="larger", thr=0)
        opthandler.register_check("LearningRate", check="larger", thr=0)
        opthandler.register_check("ValidationSize", check="larger", thr=0)
        opthandler.register_check("Optimiser", check="inlist", lst=["Adadelta", "Adam", "Adamax", "Nadam", "RMSprop"])
        opthandler.register_check("Loss", check="set")
        opthandler.register_check("Loss", check="inlist", lst=["binary_crossentropy", "categorical_crossentropy"])
        opthandler.register_check("Patience", check="larger", thr=0)
        opthandler.register_check("MinDelta", check="larger", thr=0)
        opthandler.register_check("RestoreBest", check="inlist", lst=[True, False])
        return opthandler

    def _initBDTModel(self):
        """Initialise method for BDTMODEL block

        :returns:

        """
        opthandler = self._initBasicModel()
        opthandler.setdescr("BDTMODEL")
        opthandler.register("nEstimators", 100)
        opthandler.register("MaxDepth", 3)
        opthandler.register("MaxFeatures", None)
        opthandler.register("MinSamplesSplit", 2)
        opthandler.register("MinSamplesLeaf", 1)
        opthandler.register("Patience", None)
        opthandler.register("MinDelta", None)
        opthandler.register("Criterion", "friedman_mse")
        opthandler.register("Loss", None)
        opthandler.register("ReweightTargetDistribution", False)

        opthandler.register_check("MaxDepth", check="larger", thr=0)
        opthandler.register_check("MinSamplesSplit", check="larger", thr=0)
        opthandler.register_check("MinSamplesLeaf", check="larger", thr=0)
        opthandler.register_check("MinDelta", check="larger", thr=0)
        opthandler.register_check("Patience", check="larger", thr=0)
        opthandler.register_check("Loss", check="inlist", lst=["deviance", "log_loss", "squared_error", "absolute_error"])
        opthandler.register_check("Criterion", check="inlist", lst=["friedman_mse", "mse"])
        opthandler.register_check("ReweightTargetDistribution", check="inlist", lst=[True, False])
        return opthandler

    def _prep(self):
        """Preping the lines in the config files and cleaning them up.

        """

        # TODO find a more elegant way to do this
        ids = ["GENERAL",
               "DNNMODEL",
               "BDTMODEL",
               "GNNMODEL",
               "SAMPLE",
               "VARIABLE",
               "OUTPUT",
               "GRAPHNODE"]

        with open(self.m_fname, encoding="utf-8") as f:
            # self.m_lnumbers = np.arange(1,len([line.rstrip() for line in f])+1,1)
            ctr = 0
            for line in f:
                line = line.split("#", 1)[0]
                line = line.replace(" ", "").replace("\t", "")
                ctr += 1
                if not len(line) > 0:
                    continue
                if line in ["\n", "\r\n"]:
                    continue
                self.m_lnumbers.append(ctr)

        with open(self.m_fname, encoding="utf-8") as f:
            lns = (line.rstrip() for line in f)
            lns = list(line for line in lns if line)  # Non-blank lns
            lns = list(line.replace("\t", "") for line in lns)  # strip tabs
            lns = list(line.split("#", 1)[0].rstrip()
                       for line in lns)  # ignore comments
            lns = [l for l in lns if len(l) > 0]  # ignore empty lines
            lns = [l.replace("\\", "#") for l in lns]

        # using list comprehension + zip() + slicing + enumerate()
        # Split list into lists by particular value
        idx_list = [idx for idx, val in enumerate(lns) if val in ids]
        self.m_blocks = [lns[i: j]
                         for i, j in zip(idx_list, idx_list[1:] + [len(lns)])]
        self.m_lnumbers = [self.m_lnumbers[i: j]
                           for i, j in zip(idx_list, idx_list[1:] + [len(self.m_lnumbers)])]

    def _build(self):
        """Reading the input blocks and building dictionary of optionhandler blocks.

        :returns:

        """
        for block, lnumbers in zip(self.m_blocks, self.m_lnumbers):
            if "MODEL" in block[0]:
                self.m_mname = block[0]
            if block[0] in self.m_cfgvals:
                if not isinstance(self.m_cfgvals[block[0]], list):
                    self.m_cfgvals[block[0]] = [self.m_cfgvals[block[0]]]
                self.m_cfgvals[block[0]].append(self._fill(block[1:], self._initBlock(block[0]), lnumbers))
            else:
                self.m_cfgvals[block[0]] = self._fill(block[1:], self._initBlock(block[0]), lnumbers)

    def _blockcheck(self):
        """Check if necessary blocks are present.

        :returns:

        """
        for s in ["OUTPUT", "MODEL", "VARIABLE", "GENERAL"]:
            try:
                self.get(s)
            except KeyError:
                ErrorMessage(f"No {s} block detected!")

    def _sanitychecks(self):
        """perform sanity checks

        :returns:

        """
        self._checkuniquevars()

    def _checkuniquevars(self):
        """Check if all variables have a unique name

        :returns:

        """
        vnames = [v.get("Name") for v in self.m_cfgvals["VARIABLE"]]
        if len(np.unique(vnames)) != len(vnames):
            ErrorMessage("\n".join([f"VARIABLE {x} exists multiple times" for x in set(vnames) if vnames.count(x) > 1]))

    def read(self):
        """Reading config file and performing the parsing.

        :returns:

        """
        self._prep()
        self._build()
        self._blockcheck()
        self._sanitychecks()
        if not isinstance(self.m_cfgvals["OUTPUT"], list):
            self.m_cfgvals["OUTPUT"] = [self.m_cfgvals["OUTPUT"]]
        self.m_cfgvals["DIR"] = directoryhandler(f"{self.m_cfgvals['GENERAL'].get('Job')}")

    def Print(self):
        """Simple print function

        :returns:

        """
        for _, item in self.m_cfgvals.items():
            if isinstance(item, list):
                for i in item:
                    i.Print(print)
            else:
                item.Print(print)

    def get(self, block):
        """Getter method for option in a given block

        :param block: Block in the config file. E.g. GENERAL
        :returns:

        """

        if block == "MODEL":
            return self.m_cfgvals[self.m_mname]
        if block == "SAMPLE" and not isinstance(self.m_cfgvals[block], list):
            return [self.m_cfgvals[block]]
        return self.m_cfgvals[block]
