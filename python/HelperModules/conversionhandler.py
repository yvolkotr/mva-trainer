"""
Todo add module description.
"""
import glob
import os

import HelperModules.helperfunctions as hf
import numpy as np
import pandas as pd
import uproot
import awkward as ak
from HelperModules.messagehandler import (ConverterDoneMessage,
                                          ConverterMessage, ErrorMessage,
                                          WarningMessage)


def sanitize_root(varlist, fill_value=np.nan):
    """Transforms vector variables into a tuple of variable name and fill value.

    This is required for `root2array`, since vectorized variables otherwise
    would result in a numpy array with `dtype=object`, which lacks certain
    properties required for some numpy functions (e.g. it does not have a
    shape).

    For this, every variable containing `[n]`, where `n` is an integer will be
    transformed into a tuple `(varname, fill_value)`, while other variables are
    left unchanged. Refer to http://scikit-hep.org/root_numpy/reference/generated/root_numpy.root2array.html
    for details.

    :param varlist: List of variable names to be retrived by `uproot` later on.
    :param fill_value: numeric, list of numeric, default = numpy.nan. Value to fill retrieved vector variables with if they exceed the index
        in the ntuple. If this is a list, `len(varlist) == len(fill_value)` is required.
    :returns: List of sanitized variable names as specified above.

    """
    # Need to check whether this is still needed when using uproot

    islist_fill_value = not isinstance(fill_value, (int, float))
    varresults = []

    if islist_fill_value and not len(varlist) == len(fill_value):
        ErrorMessage("sanitize_root: If fill-value is a list, it must have the same length as varlist!")

    for i, var in enumerate(varlist):
        varparts = var.split('[')
        # We need an opening bracket (but might have a vector of vectors)
        if not len(varparts) > 1:
            varresults.append(var)
            continue
        # Check if the last bracket actually closes again exactly once
        varparts = varparts[-1].split(']')
        if not len(varparts) == 2:
            varresults.append(var)
            continue
        # Check if there is only an index between the brackets
        if not varparts[0].isdigit():
            varresults.append(var)
            continue

        # Variable seems to be an array element, let's generate a tuple!
        if islist_fill_value:
            varresults.append((var, fill_value[i]))
        else:
            varresults.append((var, fill_value))

    return varresults


def matched(string):
    """Function to check whether all brackets in string are matched.

    :param string: input string
    :returns: True of brackets are matched, False otherwise

    """

    count = 0
    for i in string:
        if i == "(":
            count += 1
        elif i == ")":
            count -= 1
        if count < 0:
            return False
    return count == 0


class conversionhandler():
    """
    Class handling the conversion of root files to pandas DataFrame objects.
    For this uproot is used.
    See here for more information on uproot: https://uproot.readthedocs.io/en/latest/basic.html
    """

    def __init__(self, cfgset):
        self.m_cfgset = cfgset
        self.m_df_all = {"x": [],
                         "y": [],
                         "weight": [],
                         "name": [],
                         "type": [],
                         "isnominal": [],
                         "foldsplit": []}
        self.m_metadata = {"tname": None,
                           "selection": "",
                           "mcweight": "",
                           "sanevariables": None,
                           "fpaths": [],
                           "emptyframes": 0,
                           "emptysamples": 0}

    def _getfoldsplit(self, fpath, tname, selection):
        """Function to return a pandas DataFrame containing identifiers to identify the fold.

        :returns: pandas Dataframe

        """
        try:
            xuncut = uproot.open(f"{fpath}:{tname}")
            fsplitvar = xuncut.arrays(self.m_cfgset.get("GENERAL").get("FoldSplitVariable"),
                                      cut=None if selection == "" else selection,
                                      library="pd",
                                      entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
            return fsplitvar
        except FileNotFoundError as e:
            ErrorMessage(f"Could not find file {fpath}. {e.args[0]}")
        except KeyError as e:
            ErrorMessage(f"Could not find 'FoldSplitVariable' ({self.m_cfgset.get('GENERAL').get('FoldSplitVariable')}) in {fpath} : {tname}. {e.args[0]}")
        return None

    def _sizewarning(self, x, fpath):
        """Check whether a root file contributes an unusual amount of events.

        :param x: input data
        :param fpath: file path
        :returns:

        """
        if len(x) == 0:
            WarningMessage(f"No events added from {fpath}")
            self.m_metadata["emptyframes"] += 1
            return
        if len(x) < 50:
            WarningMessage(f"Less than 50 events added from {fpath}")
            return

    def _getx(self, fpath, tname, variables, selection):
        """Getter function for input variables.

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing input data

        """
        if not os.path.exists(fpath):
            ErrorMessage(f"{fpath} does not seem to exist. You need to check this!")
        try:
            xuncut = uproot.open(f"{fpath}:{tname}")
        except FileNotFoundError as e:
            ErrorMessage(f"Could not find file {fpath}. {e.args[0]}")
        try:
            x = ak.to_dataframe(xuncut.arrays(variables,
                                              cut=None if selection == "" else selection,
                                              library="ak",
                                              entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly")))
            x.reset_index(drop=True)
        except uproot.exceptions.KeyInFileError as e:
            ErrorMessage(f"The variable '{e.args[0]}' is missing in tree '{tname}' in file '{fpath}'. Consider using the 'IgnoreTreenames' option.")
        for variable in variables:
            if x[variable].isnull().values.any():
                ErrorMessage(f"Variable {variable} has nan entries!")
        self._sizewarning(x, fpath)
        return x

    def _gety(self, fpath, tname, selection, sample, ylen=None):
        """Getter function for targets.

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing targets:

        """
        if isinstance(sample.get("Target"), str):
            yuncut = uproot.open(f"{fpath}:{tname}")
            y = yuncut.arrays(sample.get("Target"),
                              cut=None if selection == "" else selection,
                              library="pd",
                              entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
            y[y < 0] = np.nan
            return y.rename(columns={sample.get("Target"): "Label"})
        y = pd.DataFrame(data=np.array([sample.get("Target")] * ylen), columns=["Label"])
        y[y < 0] = np.nan
        return y

    def _getw(self, fpath, tname, selection, sample, wlen=None):
        """Getter function for weights

        :param fpath: File path to root file
        :param tname: Tree name of TTree object within that root file
        :param variables: List of variables to be exctracted
        :param selection: Selection string obeying python logic
        :returns: Pandas DataFrame object containing weights

        """
        if self.m_metadata["mcweight"] in ["", "1"] or sample.get("Type") == "Data":
            return pd.DataFrame(data=np.array([1.0] * wlen), columns=["Weight"])
        wuncut = uproot.open(f"{fpath}:{tname}")
        w = wuncut.arrays(self.m_metadata["mcweight"],
                          cut=None if selection == "" else selection,
                          library="pd",
                          entry_stop=self.m_cfgset.get("GENERAL").get("DoNOnly"))
        return w.rename(columns={self.m_metadata["mcweight"]: "Weight"})

    def _getname(self, sample, namelen=None):
        """Getter function for sample names.

        :param sample: optionhandler object describing an individual sample.
        :param namelen: default length to use for copying the sample name.
        :returns: Pandas DataFrame object containing sample names.

        """
        N = [sample.get("Name")] * namelen
        return pd.DataFrame(data=N, columns=["Sample_Name"])

    def _gettype(self, sample, typelen=None):
        """Getter function for sample names.

        :param sample: optionhandler object describing an individual sample.
        :param typelen: default length to use for copying the sample type.
        :returns: Pandas DataFrame object containing sample types.

        """
        return pd.DataFrame(data=[sample.get("Type")] * typelen, columns=["Sample_Type"])

    def _getisnominal(self, sample, nomlen=None):
        """Getter function to determine whether a sample is a nominal sample

        :param sample: optionhandler object describing an individual sample.
        :param typelen: default length to use for copying the sample isnominal identifiers.
        :returns: Pandas DataFrame object containing isnominal identifiers.

        """

        if sample.get("Type") == "Data":
            isNominal = np.array([True] * nomlen)
        if sample.get("Type") == "Systematic":
            isNominal = np.array([False] * nomlen)
        else:
            isNominal = np.array([True] * nomlen)
        return pd.DataFrame(data=isNominal, columns=["isNominal"])

    def _initialise(self, sample):
        """Method to initialise a bunch of things based on information given in sample optionhandler object.

        :param sample: optionhandler object containing sample information
        :returns:

        """
        ConverterMessage(f"Processing {sample.get('Name')} ({sample.get('Type')})")
        # TODO check if this is necessary
        ### Resetting DataFrames ###
        self.m_df_all = {k: [] for k in self.m_df_all}
        ### setting sample MC weight and selection ###
        ### Resetting some Variables ###
        if sample.get("Treename") is not None:
            self.m_metadata["tname"] = sample.get("Treename")
        else:
            self.m_metadata["tname"] = self.m_cfgset.get("GENERAL").get("Treename")
        self._setmcweight(sample)
        self._setselection(sample)
        self._checklogic()
        ### sanitize the variablle inputs ###
        self.m_metadata["sanevariables"] = sanitize_root([v.get("Name") for v in self.m_cfgset.get("VARIABLE")])
        ### get list of filepaths ###
        self.m_metadata["fpaths"] = []
        for fstring in sample.get("NtupleFiles"):
            lst = glob.glob(hf.addslash(self.m_cfgset.get("GENERAL").get("NtuplePath")) + fstring)
            if len(lst) == 0:
                ErrorMessage(f"Could not find any file with pattern {fstring} in {hf.addslash(self.m_cfgset.get('GENERAL').get('NtuplePath'))}.")
            self.m_metadata["fpaths"].extend(lst)

        for fpath in self.m_metadata["fpaths"]:
            if not os.path.isfile(fpath):
                ErrorMessage(f"Could not open {fpath}. File does not exist")

    def _setmcweight(self, sample):
        """Sets the sample weight as a combiniation of the GENERAL MC weight and the sample specific MC weight.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        if sample.get("MCWeight") != "":
            self.m_metadata["mcweight"] = f"{self.m_cfgset.get('GENERAL').get('MCWeight')}*{sample.get('MCWeight')}"
        else:
            self.m_metadata["mcweight"] = self.m_cfgset.get("GENERAL").get("MCWeight")

    def _setselection(self, sample):
        """Sets the sample selection as a combiniation of the GENERAL selection and the sample specific selection.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        if sample.get("Selection") != "":
            if self.m_cfgset.get('GENERAL').get('Selection') == "":
                self.m_metadata["selection"] = f"({sample.get('Selection')})"
            else:
                self.m_metadata["selection"] = f"({self.m_cfgset.get('GENERAL').get('Selection')})&({sample.get('Selection')})"
        else:
            self.m_metadata["selection"] = self.m_cfgset.get("GENERAL").get("Selection")

    def _checkmatched(self):
        """Checker function applying the matched method to the sample selection and mc weight.

        :returns:

        """
        if not matched(self.m_metadata["selection"]):
            ErrorMessage(f"Sample selection has unmatched brackets! {self.m_metadata['selection']}")
        if not matched(self.m_metadata["mcweight"]):
            ErrorMessage(f"Sample selection has unmatched brackets! {self.m_metadata['mcweight']}")

    def _checkcpp(self):
        """Checker function to look for possible c++ characters.

        :returns:

        """
        if any(item in self.m_metadata["selection"] for item in ["@", "->", "^"]):
            ErrorMessage(f"Found c++ style character in selection! {self.m_metadata['selection']}")
        if any(item in self.m_cfgset.get("GENERAL").get("MCWeight") for item in ["@", "->", "^"]):
            ErrorMessage(f"Found c++ style character in weight! {self.m_metadata['mcweight']}")
        if "&&" in self.m_metadata["selection"]:
            ErrorMessage("Found c++ style '&&' in combined selection string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_metadata["selection"]:
            ErrorMessage("Found c++ style '||' in combined selection string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_metadata["selection"].find("!")
        if self.m_metadata["selection"] != "" and idx != -1 and self.m_metadata["selection"][idx + 1] != "=":
            ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_metadata["selection"].find("!", idx + 1)
            if self.m_metadata["selection"][idx + 1] != "=" and idx != -1:
                ErrorMessage("Found c++ style not operator '!' in combined selection string. Please use != in a comparison instead.")
        if "&&" in self.m_metadata["mcweight"]:
            ErrorMessage("Found c++ style '&&' in combined MCWeight string. For a proper conversion please use (statement A)&(statement B) instead.")
        if "||" in self.m_metadata["mcweight"]:
            ErrorMessage("Found c++ style '||' in combined MCWeight string. For a proper conversion please use (statement A)|(statement B) instead.")
        idx = self.m_metadata["mcweight"].find("!")
        if idx != -1 and self.m_metadata["mcweight"][idx + 1] != "=":
            ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")
        while idx != -1:
            idx = self.m_metadata["mcweight"].find("!", idx + 1)
            if self.m_metadata["mcweight"][idx + 1] != "=" and idx != -1:
                ErrorMessage("Found c++ style not operator '!' in combined MCWeight string. Please use != in a comparison instead.")

    def _checklogic(self):
        """Combining the c++ check and the matched check

        :returns:

        """
        self._checkmatched()
        self._checkcpp()

    def _finalise(self, sample):
        """Finalising the conversion of a sample by concatenating all pandas DataFrames.

        :param sample: optionhandler object containing sample information.
        :returns:

        """
        ConverterMessage(f"Finalising conversion for {sample.get('Name')} ({sample.get('Type')})")
        # Adding all temporary lists together
        for k, _ in self.m_df_all.items():
            if len(self.m_df_all[k]) == 0:
                continue
            self.m_df_all[k] = pd.concat(self.m_df_all[k])
            self.m_df_all[k] = self.m_df_all[k].reset_index(drop=True)
        df_all = pd.concat(self.m_df_all.values(), axis=1, sort=False)
        df_all.to_hdf(f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}{sample.get('Name')}.h5", key="df", mode="w")
        ConverterDoneMessage(f"Processing {sample.get('Name')} ({sample.get('Type')}) DONE!")

    def _merge(self):
        """Simple function to merge the .h5 datasets which were created together into one big dataset containing everything.

        :returns:

        """
        if self.m_metadata["emptysamples"] == len(self.m_cfgset.get("SAMPLE")):
            ErrorMessage("None of the given Samples has events passing the respective selection! You need to check this!")
        ConverterMessage("Merging datasets...")
        dfs = [pd.read_hdf(fname) for fname in [f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}{s.get('Name')}.h5" for s in self.m_cfgset.get("SAMPLE")]]
        merged = pd.concat(dfs)
        # Shuffle the merged dataframes in place and reset index. Use random_state for reproducibility
        seed = 1
        merged = merged.sample(frac=1, random_state=seed).reset_index(drop=True)
        merged.to_hdf(f"{hf.addslash(self.m_cfgset.get('DIR').get('Data'))}merged.h5", key="df", mode="w")
        ConverterDoneMessage(f"Merged hdf5 file (data) written to {hf.addslash(self.m_cfgset.get('DIR').get('Data'))}merged.h5")

    def convert(self):
        """Perform the conversion of a given file.

        :returns:

        """

        # TODO add trigger for only doing plots
        # TODO handle situation with/without control plots
        for sample in self.m_cfgset.get("SAMPLE"):
            self._initialise(sample)
            for fpath in self.m_metadata["fpaths"]:
                self.m_df_all["x"].append(self._getx(fpath=fpath,
                                                     tname=self.m_metadata["tname"],
                                                     variables=self.m_metadata["sanevariables"],
                                                     selection=self.m_metadata["selection"]))
                l = len(self.m_df_all["x"][-1])
                self.m_df_all["y"].append(self._gety(fpath=fpath,
                                                     tname=self.m_metadata["tname"],
                                                     selection=self.m_metadata["selection"],
                                                     sample=sample,
                                                     ylen=l))
                self.m_df_all["weight"].append(self._getw(fpath=fpath,
                                                          tname=self.m_metadata["tname"],
                                                          selection=self.m_metadata["selection"],
                                                          sample=sample,
                                                          wlen=l))
                self.m_df_all["name"].append(self._getname(sample=sample,
                                                           namelen=l))
                self.m_df_all["type"].append(self._gettype(sample=sample,
                                                           typelen=l))
                self.m_df_all["isnominal"].append(self._getisnominal(sample=sample,
                                                                     nomlen=l))
                self.m_df_all["foldsplit"].append(self._getfoldsplit(fpath=fpath,
                                                                     tname=self.m_metadata["tname"],
                                                                     selection=self.m_metadata["selection"]))
            self._finalise(sample)
        self._merge()
