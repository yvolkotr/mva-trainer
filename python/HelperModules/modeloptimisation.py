"""Module to handle optimisation of models

"""

import os
import numpy as np
import HelperModules.helperfunctions as hf
from HelperModules.messagehandler import OptimiserMessage, ErrorMessage


class modeloptimisation():
    """Class handling the optimisation of models
    by defining config files (e.g. for HTCondor).

    """

    def __init__(self, args, cfgset):
        self.m_args = args
        self.m_cfgset = cfgset
        if self.m_args.optimisationpath is None:
            ErrorMessage("You need to provide --optimisationpath")
        else:
            if os.path.isdir(self.m_args.optimisationpath):
                os.system(f"rm -rf {os.path.abspath(self.m_args.optimisationpath)}")
            os.mkdir(self.m_args.optimisationpath)

    def getdnnpardict(self):
        """Function to generate a parameter dictionary containing relevant optimisation information for a DNN

        :returns: Parameter dictionary

        """
        smodel = self.m_cfgset.get("MODEL")
        d = {"MinLayers": smodel.get("MinLayers"),
             "MaxLayers": smodel.get("MaxLayers"),
             "Nodes": range(smodel.get("MinNodes"), smodel.get("MaxNodes"), smodel.get("StepNodes")),
             "SetActivationFunctions": smodel.get("SetActivationFunctions"),
             "LearningRate": np.arange(smodel.get("MinLearningRate"), smodel.get("MaxLearningRate") + smodel.get("StepLearningRate"), smodel.get("StepLearningRate"))}

        modeldict = {}
        for iteration in range(self.m_args.nModels):
            modeldict[f"P{iteration}"] = {}
            nodes = [str(item) for item in np.random.choice(d["Nodes"], np.random.randint(d["MinLayers"], d["MaxLayers"], 1), replace=True).tolist()]
            layers = len(nodes)
            choicearr = range(0, layers)
            dropoutsize = np.random.randint(0, layers, 1)
            batchnormsize = np.random.randint(0, layers, 1)
            modeldict[f"P{iteration}"]["Nodes"] = ",".join(nodes)
            modeldict[f"P{iteration}"]["DropoutIndece"] = ",".join([str(item) for item in sorted(np.random.choice(choicearr, dropoutsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["BatchNormIndece"] = ",".join([str(item) for item in sorted(np.random.choice(choicearr, batchnormsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["ActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers)]])
            if not modeldict[f"P{iteration}"]["DropoutIndece"]:
                modeldict[f"P{iteration}"]["DropoutIndece"] = "-1"
            if not modeldict[f"P{iteration}"]["BatchNormIndece"]:
                modeldict[f"P{iteration}"]["BatchNormIndece"] = "-1"
        return modeldict

    def getgnnpardict(self):
        """Function to generate a parameter dictionary containing relevant optimisation information for a GNN

        :returns: Parameter dictionary

        """
        smodel = self.m_cfgset.get("MODEL")
        d = {"MinLayers": smodel.get("MinLayers"),
             "MaxLayers": smodel.get("MaxLayers"),
             "Nodes": range(smodel.get("MinNodes"), smodel.get("MaxNodes"), smodel.get("StepNodes")),
             "SetActivationFunctions": smodel.get("SetActivationFunctions"),
             "SetGraphLayers": smodel.get("SetGraphLayers"),
             "LearningRate": np.arange(smodel.get("MinLearningRate"), smodel.get("MaxLearningRate") + smodel.get("StepLearningRate"), smodel.get("StepLearningRate"))}

        modeldict = {}
        for iteration in range(self.m_args.nModels):
            modeldict[f"P{iteration}"] = {}
            globalnodes = [str(item) for item in np.random.choice(d["Nodes"], np.random.randint(d["MinLayers"], d["MaxLayers"], 1), replace=True).tolist()]
            finalnodes = [str(item) for item in np.random.choice(d["Nodes"], np.random.randint(d["MinLayers"], d["MaxLayers"], 1), replace=True).tolist()]
            graphchannels = [str(item) for item in np.random.choice(d["Nodes"], np.random.randint(d["MinLayers"], d["MaxLayers"], 1), replace=True).tolist()]
            layers_globalnodes = len(globalnodes)
            layers_finalnodes = len(finalnodes)
            layers_graphchannels = len(graphchannels)
            modeldict[f"P{iteration}"]["Name"] = smodel.get("Name") + f"_P{iteration}"
            modeldict[f"P{iteration}"]["GlobalNodes"] = ",".join(globalnodes)
            modeldict[f"P{iteration}"]["FinalNodes"] = ",".join(finalnodes)
            modeldict[f"P{iteration}"]["GraphChannels"] = ",".join(graphchannels)
            modeldict[f"P{iteration}"]["GraphLayers"] = ",".join([str(item) for item in [np.random.choice(d["SetGraphLayers"]) for _ in range(layers_graphchannels)]])
            modeldict[f"P{iteration}"]["GraphActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_graphchannels)]])
            modeldict[f"P{iteration}"]["GlobalActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_globalnodes)]])
            modeldict[f"P{iteration}"]["FinalActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers_finalnodes)]])
        return modeldict

    def getbdtpardict(self):
        """Function to generate a parameter dictionary containing relevant optimisation information for a BDT

        :returns: Parameter dictionary

        """
        smodel = self.m_cfgset.get("MODEL")
        d = {"MaxFeatures": smodel.get("MinLayers"),
             "MaxDepth": smodel.get("MaxLayers"),
             "LearningRate": smodel.get("LearningRate")}
        modeldict = {}
        for iteration in range(self.m_args.nModels):
            modeldict[f"P{iteration}"] = {}
            nodes = [str(item) for item in np.random.choice(d["Nodes"], np.random.randint(d["MinLayers"], d["MaxLayers"], 1), replace=True).tolist()]
            layers = len(nodes)
            choicearr = range(0, layers)
            dropoutsize = np.random.randint(0, layers, 1)
            batchnormsize = np.random.randint(0, layers, 1)
            modeldict[f"P{iteration}"]["Nodes"] = ",".join(nodes)
            modeldict[f"P{iteration}"]["DropoutIndece"] = ",".join([str(item) for item in sorted(np.random.choice(choicearr, dropoutsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["BatchNotmIndece"] = ",".join([str(item) for item in sorted(np.random.choice(choicearr, batchnormsize, replace=False).tolist())])
            modeldict[f"P{iteration}"]["ActivationFunctions"] = ",".join([str(item) for item in [np.random.choice(d["SetActivationFunctions"]) for _ in range(layers)]])
            if not modeldict[f"P{iteration}"]["DropoutIndece"]:
                modeldict[f"P{iteration}"]["DropoutIndece"] = "-1"
            if not modeldict[f"P{iteration}"]["BatchNormIndece"]:
                modeldict[f"P{iteration}"]["BatchNormIndece"] = "-1"
        return modeldict

    def writeconfigs(self):
        """Method to write functions to disk.

        :returns:

        """
        if self.m_cfgset.get("GENERAL").get("RandomSeed") is not None:
            np.random.seed(self.m_cfgset.get("GENERAL").get("RandomSeed"))
        if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
            pardict = self.getdnnpardict()
        if "BDT" in self.m_cfgset.get("MODEL").get("Type"):
            pardict = self.getbdtpardict()
        if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
            pardict = self.getgnnpardict()

        for iteration in range(self.m_args.nModels):
            OptimiserMessage(f"Generating HTCondor (P{iteration}) config file in {hf.addslash(os.path.abspath(self.m_args.optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}")
            fcfg = open(f"{hf.addslash(os.path.abspath(self.m_args.optimisationpath))}{self.m_cfgset.get('MODEL').get('Name')}_P{iteration}.cfg", "w", encoding="utf-8")
            # GENERAL block
            fcfg.write("GENERAL\n")
            for key, opt in self.m_cfgset.get("GENERAL").getopts().items():
                if opt.is_set():
                    fcfg.write(f"\t{opt.getostr()}\n")
            fcfg.write("\n")
            for sample in self.m_cfgset.get("SAMPLE"):
                fcfg.write("SAMPLE\n")
                for key, opt in sample.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")
            for variable in self.m_cfgset.get("VARIABLE"):
                fcfg.write("VARIABLE\n")
                for key, opt in variable.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")
            if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
                for variable in self.m_cfgset.get("GRAPHNODE"):
                    fcfg.write("GRAPHNODE\n")
                    for key, opt in variable.getopts().items():
                        if opt.is_set():
                            fcfg.write(f"\t{opt.getostr()}\n")
                    fcfg.write("\n")
            for variable in self.m_cfgset.get("OUTPUT"):
                fcfg.write("OUTPUT\n")
                for key, opt in variable.getopts().items():
                    if opt.is_set():
                        fcfg.write(f"\t{opt.getostr()}\n")
                fcfg.write("\n")

            if "DNN" in self.m_cfgset.get("MODEL").get("Type"):
                fcfg.write("DNNMODEL\n")
            if "BDT" in self.m_cfgset.get("MODEL").get("Type"):
                fcfg.write("BDTMODEL\n")
            if "GNN" in self.m_cfgset.get("MODEL").get("Type"):
                fcfg.write("GNNMODEL\n")

            for key, opt in self.m_cfgset.get("MODEL").getopts().items():
                if opt.is_set():
                    if key in pardict[f"P{iteration}"].keys():
                        fcfg.write(f"\t{key} = {pardict[f'P{iteration}'][key]}\n")
                    else:
                        fcfg.write(f"\t{opt.getostr()}\n")
            fcfg.close()
            fsub = open(f"{hf.addslash(self.m_args.optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}.sub", "w", encoding="utf-8")
            fsub.write("###################\n")
            fsub.write("### Job Options ###\n")
            fsub.write("###################\n")
            fsub.write("executable\t\t= $ENV(MVA_TRAINER_BASE_DIR)/scripts/HTCondorScripts/HTCondorMain.sh\n")
            fsub.write(f"arguments\t\t= {self.m_args.HO_options} {hf.addslash(self.m_args.optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}P$(ProcId).cfg\n")
            fsub.write("request_memory\t\t= 2000\n")
            fsub.write("+MaxRuntime\t\t= 28800\n")  # Setting the maximum run time to 8 hours (because this does not exist as an independent flavour)
            fsub.write("max_retries\t\t= 10\n")
            fsub.write("requirements\t\t= Machine =!= LastRemoteHost\n")
            fsub.write("on_exit_remove\t\t= (ExitBySignal == False) && ((ExitCode == 0)\n")
            fsub.write(f'+JobBatchName\t\t= {self.m_cfgset.get("MODEL").get("Name")}_P$(ProcId)\n')
            fsub.write("\n")
            fsub.write("###################\n")
            fsub.write("### OutputFiles ###\n")
            fsub.write("###################\n")
            fsub.write(f"log\t\t= {hf.addslash(self.m_args.optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_$(ClusterId)_P$(ProcId).log\n")
            fsub.write(f"error\t\t= {hf.addslash(self.m_args.optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_$(ClusterId)_P$(ProcId)_error.txt\n")
            fsub.write(f"output\t\t= {hf.addslash(self.m_args.optimisationpath)}{self.m_cfgset.get('MODEL').get('Name')}_$(ClusterId)_P$(ProcId)_$(ClusterId)_P$(ProcId)_out.txt\n")
            fsub.write("getenv\t\t= True\n")
            fsub.write("stream_error\t\t= True\n")
            fsub.write("stream_output\t\t= True\n")
            fsub.write('requirements\t\t= OpSysAndVer == "CentOS7"\n')
            fsub.write("should_transfer_files\t= YES\n")
            fsub.write("when_to_transfer_output\t= ON_EXIT\n")
            fsub.write("\n")
            fsub.write("#####################\n")
            fsub.write("### Notifications ###\n")
            fsub.write("#####################\n")
            fsub.write(f"#notify_user\t\t= {os.environ.get('USER')}@cern.ch\n")
            fsub.write("#notification\t\t= always\n")
            fsub.write(f"queue {self.m_args.nModels}\n")
            fsub.close()
