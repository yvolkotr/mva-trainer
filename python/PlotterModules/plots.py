"""Module holding the relevant methods for all plots.

"""
import copy
import numpy as np
from HelperModules.messagehandler import (ErrorMessage, InfoMessage,
                                          WarningMessage)
from PlotterModules.AtlasLabel import CustomLabel, DrawLabels
from ROOT import TCanvas, TGraph, TH1D, TH2D, THStack, TLegend, TMultiGraph, TPad, gPad, gStyle, kRed, kBlue
from sklearn import metrics


def ensure_decreasing(arr1, arr2):
    """The function takes two arrays as input, makes a copy of each,
    and iterates through the arrays. If the values in either array
    are not decreasing, the function deletes the current element from
    both arrays using the del statement. If the values are decreasing,
    the function increments the index i.

    :param arr1: First array
    :param arr2: Second array
    :returns: updated arrays

    """
    # Create a copy of the input arrays
    arr1_copy = arr1.copy()
    arr2_copy = arr2.copy()

    # Iterate through the arrays
    i = 1
    while i < len(arr1_copy):
        # If the values are not decreasing, delete the element from both arrays
        if arr1_copy[i] >= arr1_copy[i - 1] or arr2_copy[i] >= arr2_copy[i - 1]:
            del arr1_copy[i], arr2_copy[i]
        else:
            i += 1

    return arr1_copy, arr2_copy


def roc_curve(y_true, y_score, sampling, target, weights, verbose=True):
    """Manually determine fpr and tpr for giving sampling

    :param y_true: True targets
    :param y_score: Predicted targets
    :param sampling: Sampling rate, i.e. how many points per ROC curve are used
    :param weights: sample weights
    :returns:

    """
    cut_values = np.arange(0, 1 + 1 / sampling, 1 / sampling)
    tpr = []
    fpr = []
    if y_true.ndim != 1:
        y_true = np.squeeze(y_true)
    if y_score.ndim != 1:
        y_score = np.squeeze(y_score)
    if weights.ndim != 1:
        weights = np.squeeze(weights)
    for cut_value in cut_values:
        y_tmp = copy.deepcopy(y_score)
        y_tmp[y_tmp > cut_value] = 1.0
        y_tmp[y_tmp <= cut_value] = 0.0
        fp = weights[(y_tmp != 0) & (y_true != target)].sum()
        fn = weights[(y_tmp == 0) & (y_true == target)].sum()
        tp = weights[(y_tmp != 0) & (y_true == target)].sum()
        tn = weights[(y_tmp == 0) & (y_true != target)].sum()
        tpr.append(tp / (tp + fn))
        fpr.append(fp / (fp + tn))
    fpr, tpr = ensure_decreasing(fpr, tpr)
    if len(fpr) != sampling and verbose:
        WarningMessage(f"Detected problematic order of TPR and FPR values for ROC curve. Reducing sampling to {len(fpr)}.")
    return fpr, tpr, cut_values


def separation(hist_a, hist_b):
    """Calculating the separation between two distributions (hist_a, hist_b)

    :param hist_a: First histogram
    :param hist_b: Second Histogram
    :returns: Separation power between two histograms.

    """
    s = 0
    hist_a_bins = hist_a.GetNbinsX() + 1
    hist_b_bins = hist_b.GetNbinsX() + 1
    if hist_a_bins != hist_b_bins:
        ErrorMessage("Unequal number of bins. Can not calculate separation power!")
    else:
        for i in range(1, hist_a_bins):
            if hist_a.GetBinContent(i) + hist_b.GetBinContent(i) > 0:
                s += 1 / 2. * (hist_a.GetBinContent(i) - hist_b.GetBinContent(i))**2 / (hist_a.GetBinContent(i) + hist_b.GetBinContent(i))
            if hist_a.GetBinContent(i) < 0:
                WarningMessage("Bin content in bin " + str(i) + " of " + hist_a.GetName() + " is smaller than 0.")
            if hist_b.GetBinContent(i) < 0:
                WarningMessage("Bin content in bin " + str(i) + " of " + hist_b.GetName() + " is smaller than 0.")
    return s


def createratio(h1, h2):
    """Creates the ratio  between two histograms (h1, h2).

    :param h1: First histogram for ratio
    :param h2: Second histogram for ratio
    :returns:

    """
    if h1 is None or h2 is None:
        return None
    h3 = h1.Clone("h3")
    h3.SetTitle("")
    h3.Divide(h2)
    return h3


class basicplot():
    """Class used as a based class for every plot.

    """

    def __init__(self, plotname=None):
        self.m_plotname = plotname
        self.m_hists = {}
        self.m_canvas = TCanvas()
        self.m_legend = None
        self.m_sf = 1.4

    def initlegend(self, x1, y1, x2, y2):
        """Setting up a rudimentary legend with ATLAS-style parameters.

        :param x1:
        :param y1:
        :param x2:
        :param y2:
        :returns:

        """
        self.m_legend = TLegend(x1, y1, x2, y2)
        self.m_legend.SetBorderSize(0)
        self.m_legend.SetTextFont(42)
        self.m_legend.SetTextSize(0.04)
        self.m_legend.SetFillStyle(0)

    def addtolegend(self, item, label, style):
        """Add an item to the legend.

        :param item: ROOT item such as TH1F or TGraph
        :param label: Label of the added item
        :param style: Style character to of the added item
        :returns:

        """
        self.m_legend.AddEntry(item, label, style)

    def addhist(self, hname, hist):
        """Adds a histogram to the dictionary of hists

        :param hname: Name of the histogram
        :param hist: TH1F histogram object
        :returns:

        """
        self.m_hists[hname] = hist

    def savefig(self, fname):
        """General method to save a figure (plot) to disk.

        :param fname: Output file path
        :returns:

        """
        if isinstance(fname, list):
            for f in fname:
                if self.m_plotname is not None:
                    InfoMessage(f"Creating {self.m_plotname} - {f}")
                self.m_canvas.SaveAs(f)
        else:
            if self.m_plotname is not None:
                InfoMessage(f"Creating {self.m_plotname} - {fname}")
            self.m_canvas.SaveAs(fname)
        del self.m_canvas

    def setscale(self, axis="x", opt="Linear"):
        """Sets log scales

        :param axis: axis
        :param opt: Linear or log
        :returns:

        """
        if axis == "x" and opt == "Log":
            gPad.SetLogx()
        if axis == "y" and opt == "Log":
            gPad.SetLogy()
        if axis == "z" and opt == "Log":
            gPad.SetLogz()

    def setxlabel(self, hname, xlabel):
        """Sets the X label of histogram 'hname'.

        :param hname: Histogram for which the X label is set.
        :param xlabel: X label to be set.
        :returns:

        """
        self.m_hists[hname].GetXaxis().SetTitle(xlabel)

    def setylabel(self, hname, ylabel):
        """Sets the Y label of histogram 'hname'.

        :param hname: Histogram for which the Y label is set.
        :param ylabel: Y label to be set.
        :returns:

        """
        self.m_hists[hname].GetYaxis().SetTitle(ylabel)

    def setzlabel(self, hname, zlabel):
        """Sets the Z label of histogram 'hname'.

        :param hname: Histogram for which the the Z label is set.
        :param zlabel: Z label to be set
        :returns:

        """
        self.m_hists[hname].GetZaxis().SetTitle(zlabel)

    def setLabels(self, ATLASlabel, CMlabel, customlabel, opt="data", align="left"):
        """Setting Labels such as the ATLAS label, CME label and custom labels.

        :param ATLASlabel: Text behind ATLAS label
        :param CMlabel: CME label
        :param customlabel: Custom label
        :param opt: Can be MC or data
        :param align: where the labels are positioned.
        :returns:

        """
        DrawLabels(ATLASlabel, CMlabel, customlabel, opt, align)

    def setcustomLabel(self, x, y, customlabel):
        """Setting custom label

        :param x: x position
        :param y: y position
        :param customlabel: Custom label
        :returns:

        """
        CustomLabel(x, y, customlabel)

    def geterrband(self, in_hist, isratio=False, color=600, fstyle=3018, msize=0):
        """Building an errorband for a TH1F

        :param in_hist: Input histogram
        :param isratio: Boolean determining whether this is an errorband for a ratio.
        :param color: The color of the errorband.
        :param fstyle: The printing style of the errorband.
        :param msize: Marker size
        :returns: The errorband object (usually another histogram)

        """
        if in_hist is None:
            return None
        errband = in_hist.Clone("ratioerr" if isratio else "errband")
        if isratio:
            for i in range(errband.GetNbinsX() + 1):
                if errband.GetBinContent(i) == 0:
                    errband.SetBinError(i, 0)
                else:
                    errband.SetBinError(i, errband.GetBinError(i) / errband.GetBinContent(i))
                    errband.SetBinContent(i, 1)
        errband.SetFillColor(color)
        errband.SetFillStyle(fstyle)
        errband.SetMarkerSize(msize)
        return errband

    def _scale(self):
        """Function to scale histograms to unity

        :returns:

        """
        for h in self.m_hists:
            if "ratio" in h or "err" in h:
                continue
            if (not self.m_hists[h] is None
                and not self.m_hists[h].Integral() == 1
                    and not self.m_hists[h].Integral() == 0):
                self.m_hists[h].Scale(1. / self.m_hists[h].Integral())


class ratioplot(basicplot):
    """Class to produce a ratio plot

    """

    def __init__(self, h1, h2=None, plotname="Ratio Plot", doyields=False, blindingindece=None):
        super().__init__(plotname)
        self.addhist("h1", h1)
        self.addhist("h2", h2)
        self.addhist("ratio", createratio(h2, h1.GetStack().Last() if isinstance(h1, THStack) else h1) if h2 is not None else self._getdummyratio())
        self.addhist("up_err", self.geterrband(h1.GetStack().Last() if isinstance(h1, THStack) else h1))
        self.addhist("low_err", self.geterrband(self.m_hists["ratio"], isratio=True))
        self.m_upperpad = None
        self.m_lowerpad = None
        self.m_blindingindece = blindingindece if blindingindece is not None else []
        self.m_doyields = doyields
        self.m_upperpad = TPad("upperpad", "upperpad", 0, 0.3, 1, 1.0)
        self.m_upperpad.SetBottomMargin(0)  # joins upper and lower plot
        self.m_upperpad.Draw()
        self.m_canvas.cd()  # returns to main canvas before defining lower pad
        self.m_lowerpad = TPad("lowerpad", "lowerpad", 0, 0.05, 1, 0.3)
        self.m_lowerpad.SetTopMargin(0)  # joins upper and lower plot
        self.m_lowerpad.SetBottomMargin(0.4)
        self.m_lowerpad.Draw()
        self.m_hists["ratio"].SetMaximum(1.55)
        self.m_hists["ratio"].SetMinimum(0.45)
        self.m_hists["ratio"].GetXaxis().SetLabelSize(0.15)
        self.m_hists["ratio"].GetYaxis().SetLabelSize(0.15)
        self.m_hists["ratio"].GetXaxis().SetTitleSize(0.15)
        self.m_hists["ratio"].GetYaxis().SetTitleSize(0.15)
        self.m_hists["ratio"].GetYaxis().SetTitleOffset(0.4)
        self.initlegend(.58, .725, .95, .90)

    def _applyblinding(self):
        """Function to calculate bin indeces to be blinded

        :returns:

        """
        for bin_i in range(self.m_hists["ratio"].GetNbinsX() + 1):
            if bin_i in self.m_blindingindece:
                self.m_hists["ratio"].SetBinContent(bin_i, -9999)
                self.m_hists["h2"].SetBinContent(bin_i, -9999)

    def _getdummyratio(self):
        """Function to calculate a dummy ratio

        :returns: A dummy ratio histogram

        """
        if isinstance(self.m_hists["h1"], THStack):
            dummy = self.m_hists["h1"].GetStack().Last().Clone("ratio_dummy")
        else:
            dummy = self.m_hists["h1"].Clone("ratio_dummy")
        dummy.SetMarkerSize(0)
        return dummy

    def _drawupper(self):
        """Draws the upper pad of a ratio plot.

        :returns:

        """
        self.m_upperpad.cd()
        if not self.m_hists["h2"] is None and self.m_hists["h2"].GetName().split("_")[0] == "Data":
            if self.m_doyields:
                nevents = self.m_hists["h2"].Integral()
                self.addtolegend(self.m_hists["h2"], f"Data  {nevents:.1f}", "p")
            else:
                self.addtolegend(self.m_hists["h2"], "Data", "p")
        for hist in self.m_hists["h1"].GetHists():
            if self.m_doyields:
                nevents = hist.Integral()
                self.addtolegend(hist, f"{hist.GetName().split('_')[0]}  {nevents:.1f}", "f")
            else:
                self.addtolegend(hist, f"{hist.GetName().split('_')[0]}", "f")
        self.m_hists["h1"].SetMaximum(self.m_hists["h1"].GetMaximum() * self.m_sf)
        self.m_hists["h1"].Draw("hist")  # MC
        self.m_hists["up_err"].Draw("e2 same")
        if not self.m_hists["h2"] is None:
            self.m_hists["h2"].Draw("ep same")  # Data
        ax = self.m_hists["h1"].GetYaxis()
        # to avoid clipping the bottom zero, redraw a small axis
        ax.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

    def _drawlower(self):
        """Draws the lower pad of a ratio plot.

        :returns:

        """
        self.m_lowerpad.cd()
        self.m_hists["ratio"].Draw("ep")
        if not self.m_hists["low_err"] is None:
            self.m_hists["low_err"].Draw("e2 same")
        self.m_upperpad.cd()

    def draw(self):
        """Draws upper and lower pad of ratio plot and adds legend.

        :returns:

        """
        self._applyblinding()
        self._drawupper()
        self._drawlower()
        self.m_legend.Draw("same")


class stack2D(basicplot):
    """2D stack plot. Useful for multi-class classifiers.

    """

    def __init__(self, h1, plotname="2D Stack Plot"):
        super().__init__(plotname)
        self.addhist("h1", h1)
        self.m_canvas.SetRightMargin(0.25)
        self.m_hists["h1"].GetZaxis().SetTitleOffset(1.4)
        self.m_hists["h1"].SetMinimum(0)

    def draw(self):
        """Draw method

        :returns:

        """
        self.m_hists["h1"].Draw("colz")


class trainstats(basicplot):
    """Plot to provide information on training statistics.

    """

    def __init__(self, h1, binnames, plotname="Training Stats"):
        super().__init__(plotname)
        self.addhist("h1", h1)
        self.m_binnames = binnames
        self.m_hists["h1"].SetFillColor(4)
        self.m_hists["h1"].SetBarWidth(0.5)
        self.m_hists["h1"].SetBarOffset(0.25)  # bar is centered with width 0.5
        self.m_canvas.SetLeftMargin(0.25)

    def draw(self):
        """Draw method

        :returns:

        """
        self.m_hists["h1"].Draw("hbar")
        for bin_ID in range(self.m_hists["h1"].GetNbinsX()):
            count = f"Nr of Events: {self.m_hists['h1'].GetBinContent(bin_ID+1)}"
            name = f"{self.m_binnames[bin_ID]}"
            label = "#splitline{" + name + "}{" + count + "}"
            self.m_hists["h1"].GetXaxis().SetBinLabel(bin_ID + 1, f"{label}")
        gPad.SetLogx()
        gPad.SetGridx(1)


class negweightstats(trainstats):
    """Plot to provide information on the amount of negative weights per sample.

    """

    def __init__(self, h1, h2, binnames, plotname="Neg. Weight Stats"):
        super().__init__(h1, binnames, plotname)
        self.addhist("h2", h2)
        self.m_hists["h1"].SetFillColor(4)
        self.m_hists["h1"].SetBarWidth(0.4)
        self.m_hists["h1"].SetBarOffset(0.1)
        self.m_hists["h2"].SetFillColor(2)
        self.m_hists["h2"].SetBarWidth(0.4)
        self.m_hists["h2"].SetBarOffset(0.5)
        self.m_hists["h1"].SetMaximum(100)
        if self.m_hists["h1"].GetMaximum() > 60:
            self.m_hists["h1"].SetMaximum(140)
        for bin_ID in range(self.m_hists["h1"].GetNbinsX()):
            self.m_hists["h1"].GetXaxis().SetBinLabel(bin_ID + 1, f"{self.m_binnames[bin_ID]}")

    def draw(self):
        """Draw method

        :returns:

        """
        self.m_hists["h1"].Draw("hbar")
        self.m_hists["h2"].Draw("hbar same")
        self.initlegend(.68, .725, .95, .90)
        self.addtolegend(self.m_hists["h1"], "Yields", "f")
        self.addtolegend(self.m_hists["h2"], "W<0", "f")
        self.m_legend.Draw("same")


class nnegweightscomp(ratioplot):
    """Class for negative weight comparison plots

    """

    def __init__(self, h1, h2, h3, h4, h5, h6, plotname="Neg. Weight Comparison"):
        super().__init__(h1, h2)  # initialising the ratioplot
        self.addhist("h3", h3)
        self.addhist("h4", h4)
        self.addhist("h5", h5)
        self.addhist("h6", h6)
        self.addhist("ratio_2", createratio(self.m_hists["h3"], self.m_hists["h1"]))
        self.addhist("up_err_2", self.geterrband(self.m_hists["h3"], color=4, fstyle=3005))
        self.addhist("low_err_2", self.geterrband(self.m_hists["ratio_2"], isratio=True, color=4, fstyle=3005))
        self.addhist("ratio_3", createratio(self.m_hists["h5"], self.m_hists["h4"]))
        self.addhist("up_err_3", self.geterrband(self.m_hists["h5"], color=4, fstyle=3005))
        self.addhist("low_err_3", self.geterrband(self.m_hists["ratio_3"], isratio=True, color=4, fstyle=3005))
        self.addhist("ratio_4", createratio(self.m_hists["h6"], self.m_hists["h4"]))
        self.addhist("up_err_4", self.geterrband(self.m_hists["h6"], color=4, fstyle=3005))
        self.addhist("low_err_4", self.geterrband(self.m_hists["ratio_4"], isratio=True, color=4, fstyle=3005))
        if not self.m_hists["h1"] is None:
            self.m_hists["h1"].SetLineColor(2)  # red
            self.m_hists["h1"].SetFillColor(0)
            self.m_hists["h1"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h1"], "Signal (nominal)", "f")
        if not self.m_hists["h2"] is None:
            self.m_hists["h2"].SetLineColor(2)  # red
            self.m_hists["h2"].SetLineStyle(4)  # dashed
            self.m_hists["h2"].SetFillColor(0)
            self.m_hists["h2"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h2"], "Signal (W>0)", "f")
        if not self.m_hists["h3"] is None:
            self.m_hists["h3"].SetLineColor(2)
            self.m_hists["h3"].SetLineStyle(8)
            self.m_hists["h3"].SetFillColor(0)
            self.m_hists["h3"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h3"], "Signal (corrected)", "f")
        if not self.m_hists["h4"] is None:
            self.m_hists["h4"].SetLineColor(4)  # blue
            self.m_hists["h4"].SetFillColor(0)
            self.m_hists["h4"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h4"], "Bkg. (nominal)", "f")
        if not self.m_hists["h5"] is None:
            self.m_hists["h5"].SetLineColor(4)
            self.m_hists["h5"].SetLineStyle(4)  # dashed
            self.m_hists["h5"].SetFillColor(0)
            self.m_hists["h5"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h5"], "Bkg. (W>0)", "f")
        if not self.m_hists["h6"] is None:
            self.m_hists["h6"].SetLineColor(4)
            self.m_hists["h6"].SetLineStyle(8)
            self.m_hists["h6"].SetFillColor(0)
            self.m_hists["h6"].SetLineWidth(2)
            self.addtolegend(self.m_hists["h6"], "Bkg. (corrected)", "f")
        if not self.m_hists["ratio"] is None:
            self.m_hists["ratio"].SetLineWidth(2)
            self.m_hists["ratio"].SetFillColor(0)
            self.m_hists["ratio"].SetLineColor(2)
            self.m_hists["ratio"].SetLineStyle(2)
        if not self.m_hists["ratio_2"] is None:
            self.m_hists["ratio_2"].SetLineWidth(2)
            self.m_hists["ratio_2"].SetFillColor(0)
            self.m_hists["ratio_2"].SetLineColor(2)
            self.m_hists["ratio_2"].SetLineStyle(8)
        if not self.m_hists["ratio_3"] is None:
            self.m_hists["ratio_3"].SetLineWidth(2)
            self.m_hists["ratio_3"].SetFillColor(0)
            self.m_hists["ratio_3"].SetLineColor(4)
            self.m_hists["ratio_3"].SetLineStyle(2)
        if not self.m_hists["ratio_4"] is None:
            self.m_hists["ratio_4"].SetLineWidth(2)
            self.m_hists["ratio_4"].SetFillColor(0)
            self.m_hists["ratio_4"].SetLineColor(4)
            self.m_hists["ratio_4"].SetLineStyle(8)
        self.initlegend(.68, .65, .95, .90)

    def _drawupper(self):
        """TODO describe function

        :returns:

        """
        self.m_upperpad.cd()
        self.m_hists["h1"].SetMaximum(np.max([h.GetMaximum() if h is not None else 0.0 for _, h in self.m_hists.items()]) * self.m_sf)
        self.m_hists["h1"].Draw("hist")
        if not self.m_hists["h2"] is None:
            self.m_hists["h2"].Draw("hist same")
        if not self.m_hists["h3"] is None:
            self.m_hists["h3"].Draw("hist same")
        if not self.m_hists["h4"] is None:
            self.m_hists["h4"].Draw("hist same")
        if not self.m_hists["h5"] is None:
            self.m_hists["h5"].Draw("hist same")
        if not self.m_hists["h6"] is None:
            self.m_hists["h6"].Draw("hist same")
        ax = self.m_hists["h1"].GetYaxis()
        # to avoid clipping the bottom zero, redraw a small axis
        ax.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

    def _drawlower(self):
        """TODO describe function

        :returns:

        """
        self.m_lowerpad.cd()
        if not self.m_hists["ratio"] is None:
            self.m_hists["ratio"].Draw("hist")
        if not self.m_hists["ratio_2"] is None:
            self.m_hists["ratio_2"].Draw("hist same")
        if not self.m_hists["ratio_3"] is None:
            self.m_hists["ratio_3"].Draw("hist same")
        if not self.m_hists["ratio_4"] is None:
            self.m_hists["ratio_4"].Draw("hist same")
        self.m_upperpad.cd()

    def draw(self):
        """TODO describe function

        :returns:

        """
        self._drawupper()
        self._drawlower()
        self.m_legend.Draw("same")


class corrmatrix(basicplot):
    """Class for correlation matrices

    """

    def __init__(self, h1, plotname="Correlation Matrix"):
        super().__init__(plotname)
        self.addhist("h1", h1)
        gStyle.SetPaintTextFormat(".2f")
        self.m_hists["h1"].SetMinimum(-1.0)
        self.m_hists["h1"].SetMaximum(1.0)
        self.m_canvas.SetRightMargin(0.125)
        self.m_canvas.SetBottomMargin(0.2)
        self.m_canvas.SetLeftMargin(0.2)

    def draw(self):
        """Draw method

        :returns:

        """
        self.m_hists["h1"].Draw("colz text45")


class corrcomp(basicplot):
    """Class for comparing correlations when using different treatments of negative weights.

    """

    def __init__(self, h1, plotname="Correlation Comparison"):
        super().__init__(plotname)
        self.addhist("h1", h1)
        # TODO implement


class predtruth(ratioplot):
    """Plot to compare prediction with truth. Useful for regression tasks

    """

    def __init__(self, h1, h2, plotname="Pred. vs. Truth"):
        super().__init__(h1, h2, plotname)
        self.addhist("h1", h1)
        self.addhist("h2", h2)
        self.initlegend(.58, .725, .95, .90)
        self.m_hists["h1"].SetLineColor(2)  # red
        self.m_hists["h2"].SetLineColor(4)  # blue
        self.m_hists["h2"].SetLineStyle(2)  # dashed
        self.m_hists["h1"].SetFillColor(0)
        self.m_hists["h2"].SetFillColor(0)
        self.m_hists["h1"].SetLineWidth(2)
        self.m_hists["h2"].SetLineWidth(2)
        self.m_hists["ratio"].SetLineWidth(2)
        self.m_hists["ratio"].SetFillColor(0)
        self.addtolegend(self.m_hists["h1"], "Prediction", "F")
        self.addtolegend(self.m_hists["h2"], "Truth", "F")

    def _drawupper(self):
        """Draws the upper pad of a truth vs prediction plot.
        Overrides parent method from ratioplot.

        :returns:

        """
        self.m_upperpad.cd()
        self.m_hists["h1"].SetMaximum(self.m_hists["h1"].GetMaximum() * self.m_sf)
        self.m_hists["h1"].Draw("hist")  # prediction
        self.m_hists["up_err"].Draw("e2 same")
        self.m_hists["h2"].Draw("hist same")  # truth
        ax = self.m_hists["h1"].GetYaxis()
        # to avoid clipping the bottom zero, redraw a small axis
        ax.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

    def _drawlower(self):
        """Draws the lower pad of truth vs prediction plot.
        Overrides parent method from ratioplot.

        :returns:

        """
        self.m_lowerpad.cd()
        self.m_hists["ratio"].Draw("hist")
        if not self.m_hists["low_err"] is None:
            self.m_hists["low_err"].Draw("e2 same")
        self.m_upperpad.cd()


class sep1D(basicplot):
    """Simple 1D separation plot.

    """

    def __init__(self, h1, h2, plotname="Separation 1D"):
        super().__init__(plotname)
        self.addhist("Sig", h1)
        self.addhist("Bkg", h2)
        self.initlegend(.58, .725, .95, .90)
        self.m_hists["Sig"].SetLineColor(2)  # red
        self.m_hists["Bkg"].SetLineColor(4)  # blue
        self.m_hists["Bkg"].SetLineStyle(2)  # dashed
        self.m_hists["Sig"].SetFillColor(0)
        self.m_hists["Bkg"].SetFillColor(0)
        self.m_hists["Sig"].SetLineWidth(2)
        self.m_hists["Bkg"].SetLineWidth(2)
        self.addtolegend(self.m_hists["Sig"], "Signal", "F")
        self.addtolegend(self.m_hists["Bkg"], "Background", "F")
        self._scale()

    def _drawhistos(self):
        """Draws signal and background histograms for separation

        :returns:

        """
        self.m_hists["Sig"].Draw("hist")
        self.m_hists["Bkg"].Draw("hist same")
        self.m_hists["Sig"].SetMaximum(np.max([self.m_hists["Sig"].GetMaximum(), self.m_hists["Bkg"].GetMaximum()]) * self.m_sf)

    def draw(self):
        """Draw method

        :returns:

        """
        self._drawhistos()
        CustomLabel(0.2, 0.7, f"Separation={separation(self.m_hists['Sig'], self.m_hists['Bkg']) * 100:.2f}%")  # We want to get percent
        self.m_legend.Draw("same")


class sep2D(basicplot):
    """2D Separation plot. Essentially 1D separation but split per sample.

    """

    def __init__(self, h1, plotname="2D Separation"):
        super().__init__(plotname)
        self.addhist("h1", h1)
        self.m_canvas.SetRightMargin(0.15)

    def draw(self):
        """Draw method

        :returns:

        """
        self.m_hists["h1"].Draw("colz")
        self.m_hists["h1"].GetZaxis().SetTitle("Fraction of Events [%]")


class traintestplot(ratioplot):
    """Train-test plots. I.e. plots where train and test distributions are overlayed and compared using statistical tests.

    """

    def __init__(self, h1, h2, h3, h4, comparisontest, plotname="Kolmogorov-Smirnov Plot"):
        super().__init__(h1, h2, plotname)
        self.addhist("h3", h3)
        self.addhist("h4", h4)
        self.m_comparisontest = comparisontest
        self._scale()
        if h1 is not None and h2 is not None:
            self.addhist("ratio", createratio(h2, h1.GetStack().Last() if isinstance(h1, THStack) else h1) if h2 is not None else self._getdummyratio())
            self.addhist("up_err", self.geterrband(self.m_hists["h1"].GetStack().Last() if isinstance(self.m_hists["h1"], THStack) else self.m_hists["h1"], color=2, fstyle=3004))
            self.addhist("low_err", self.geterrband(self.m_hists["ratio"], isratio=True, color=2, fstyle=3004))
            self.m_hists["h1"].SetLineColor(2)  # red
            self.m_hists["h2"].SetLineColor(2)  # red
            self.m_hists["h1"].SetFillColor(0)
            self.m_hists["h2"].SetFillColor(0)
            self.m_hists["h1"].SetLineWidth(2)
            self.m_hists["h2"].SetLineWidth(2)
            self.m_hists["h1"].SetMarkerSize(0)
            self.m_hists["h2"].SetMarkerColor(2)
            self.m_hists["ratio"].SetFillColor(0)
            self.m_hists["ratio"].SetLineWidth(2)
            self.m_hists["ratio"].SetLineColor(2)
            self.m_hists["ratio"].SetMaximum(1.55)
            self.m_hists["ratio"].SetMinimum(0.45)
            self.m_hists["ratio"].GetXaxis().SetLabelSize(0.15)
            self.m_hists["ratio"].GetYaxis().SetLabelSize(0.15)
            self.m_hists["ratio"].GetXaxis().SetTitleSize(0.15)
            self.m_hists["ratio"].GetYaxis().SetTitleSize(0.15)
        if h3 is not None and h4 is not None:
            self.addhist("up_err_2", self.geterrband(self.m_hists["h3"].GetStack().Last() if isinstance(self.m_hists["h3"], THStack) else self.m_hists["h3"], color=4, fstyle=3005))
            self.addhist("low_err_2", None if self.m_hists["h4"] is None else self.geterrband(self.m_hists["ratio"], isratio=True, color=4, fstyle=3005))
            self.addhist("ratio_2", createratio(self.m_hists["h4"], h3.GetStack().Last() if isinstance(self.m_hists["h3"], THStack)
                                                else self.m_hists["h3"]) if self.m_hists["h4"] is not None else self._getdummyratio())
            self.m_hists["h3"].SetLineColor(4)  # blue
            self.m_hists["h4"].SetLineColor(4)  # blue
            self.m_hists["h3"].SetFillColor(0)
            self.m_hists["h4"].SetFillColor(0)
            self.m_hists["h3"].SetLineWidth(2)
            self.m_hists["h4"].SetLineWidth(2)
            self.m_hists["h3"].SetMarkerSize(0)
            self.m_hists["h4"].SetMarkerColor(4)
            self.m_hists["ratio_2"].SetFillColor(0)
            self.m_hists["ratio_2"].SetLineWidth(2)
            self.m_hists["ratio_2"].SetLineColor(4)
            self.m_hists["ratio_2"].SetMaximum(1.55)
            self.m_hists["ratio_2"].SetMinimum(0.45)
            self.m_hists["ratio_2"].GetXaxis().SetLabelSize(0.15)
            self.m_hists["ratio_2"].GetYaxis().SetLabelSize(0.15)
            self.m_hists["ratio_2"].GetXaxis().SetTitleSize(0.15)
            self.m_hists["ratio_2"].GetYaxis().SetTitleSize(0.15)

        if h1 is not None and h2 is not None:
            self.addtolegend(self.m_hists["h1"], "Sig. Test", "f")
            self.addtolegend(self.m_hists["h2"], "Sig. Train", "p")
        if h3 is not None and h4 is not None:
            self.addtolegend(self.m_hists["h3"], "Bkg. Test", "f")
            self.addtolegend(self.m_hists["h4"], "Bkg. Train", "p")

    def _drawpvalue(self):
        """Calculate the p value comparing two histograms using the user-defined test

        :returns:

        """
        if self.m_comparisontest == "Kolmogorov-Smirnov":
            ks_sig, ks_bkg = -1, -1
            if not self.m_hists["h2"] is None and not self.m_hists["h1"] is None:
                ks_sig = self.m_hists["h2"].KolmogorovTest(self.m_hists["h1"])
            if not self.m_hists["h4"] is None and not self.m_hists["h3"] is None:
                ks_bkg = self.m_hists["h4"].KolmogorovTest(self.m_hists["h3"])
            CustomLabel(0.2, 0.7, f"KS Test: Sig.(Bkg.) P={ks_sig:.3f} ({ks_bkg:.3f})")
        if self.m_comparisontest == "Anderson-Darling":
            ad_sig, ad_bkg = -1, -1
            if not self.m_hists["h2"] is None and not self.m_hists["h1"] is None:
                ad_sig = self.m_hists["h2"].KolmogorovTest(self.m_hists["h1"])
            if not self.m_hists["h4"] is None and not self.m_hists["h3"] is None:
                ad_bkg = self.m_hists["h4"].KolmogorovTest(self.m_hists["h3"])
            CustomLabel(0.2, 0.7, f"AD Test: Sig.(Bkg.) P={ad_sig:.3f} ({ad_bkg:.3f})")

    def _drawupper(self):
        """Draws the upper pad of a ratio plot.

        :returns:

        """
        self.m_upperpad.cd()
        if not self.m_hists["h1"] is None and not self.m_hists["h2"] is None:
            self.m_hists["h1"].SetMaximum(self.m_hists["h1"].GetMaximum() * self.m_sf)
            self.m_hists["h1"].Draw("hist e")
            self.m_hists["h2"].Draw("e1X0 same")
            ax = self.m_hists["h1"].GetYaxis()
        if not self.m_hists["h3"] is None and not self.m_hists["h4"] is None:
            self.m_hists["h3"].Draw("hist e same")
            self.m_hists["h4"].Draw("e1X0 same")
            ax = self.m_hists["h3"].GetYaxis()
        # to avoid clipping the bottom zero, redraw a small axis

        ax.ChangeLabel(1, -1, -1, -1, -1, -1, " ")

    def _drawlower(self):
        """Draws the lower pad of a ratio plot.

        :returns:

        """
        self.m_lowerpad.cd()
        if not self.m_hists["h1"] is None and not self.m_hists["h2"] is None:
            self.m_hists["ratio"].Draw("hist")
            self.m_hists["low_err"].Draw("e2 same")
        if not self.m_hists["h3"] is None and not self.m_hists["h4"] is None:
            self.m_hists["ratio_2"].Draw("hist same")
            self.m_hists["low_err_2"].Draw("e2 same")
        self.m_upperpad.cd()

    def draw(self):
        """Draws upper and lower pad of KS plot and adds legend.

        :returns:

        """
        self._drawupper()
        self._drawlower()
        self.m_legend.Draw("same")
        self._drawpvalue()


class perfmplot(basicplot):
    """Class to do a simple performance plot, i.e. loss curves

    """

    def __init__(self, perdict, plotname="Performance Plot"):
        super().__init__(plotname)
        self.m_perdict = perdict
        self.initlegend(.58, .725, .95, .90)

    def _drawgraphs(self):
        """Draw method

        :returns:

        """
        mg = TMultiGraph()
        for key, valuedict in self.m_perdict.items():
            xvals = np.fromiter(valuedict.keys(), dtype=float)
            yvals = np.fromiter(valuedict.values(), dtype=float)
            gr = TGraph(xvals.size, xvals.astype(np.double), yvals.astype(np.double))
            if "val_" in key:
                gr.SetLineColor(4)
                self.addtolegend(gr, "Validation", "L")
            else:
                gr.SetLineColor(2)
                self.addtolegend(gr, "Training", "L")
            gr.SetLineWidth(2)
            mg.Add(gr)
        mg.Draw("A")
        self.m_hists["mg"] = mg

    def draw(self):
        """Draw method

        :returns:

        """
        self._drawgraphs()
        self.m_legend.Draw("same")


class roccurveplot(basicplot):
    """ Creates a standard ROC curve plot

    """

    def __init__(self, valdict, sampling, target, plotname="ROC curve plot"):
        super().__init__(plotname)
        self.m_valdict = valdict
        self.m_sampling = sampling
        self.m_target = target
        self.initlegend(.48, .725, .95, .90)

    def _drawgraphs(self):
        """Draw method

        :returns:

        """
        mg = TMultiGraph()
        train_fold = 0
        test_fold = 0
        graphs = {}
        for key, valuedict in self.m_valdict.items():
            train_fpr, train_tpr, _ = roc_curve(y_true=valuedict["Train"][0].values,
                                                y_score=valuedict["Train"][1].values,
                                                sampling=self.m_sampling,
                                                target=self.m_target,
                                                weights=valuedict["Train"][2].values)
            test_fpr, test_tpr, _ = roc_curve(y_true=valuedict["Test"][0].values,
                                              y_score=valuedict["Test"][1].values,
                                              sampling=self.m_sampling,
                                              target=self.m_target,
                                              weights=valuedict["Test"][2].values)
            train_fpr = np.fromiter(train_fpr, dtype=float)
            train_tpr = np.fromiter(train_tpr, dtype=float)
            test_fpr = np.fromiter(test_fpr, dtype=float)
            test_tpr = np.fromiter(test_tpr, dtype=float)
            graphs[f"Train_{key}"] = TGraph(train_fpr.size, train_fpr.astype(np.double), train_tpr.astype(np.double))
            graphs[f"Test_{key}"] = TGraph(test_fpr.size, test_fpr.astype(np.double), test_tpr.astype(np.double))
            train_auc = metrics.auc(train_fpr, train_tpr)
            test_auc = metrics.auc(test_fpr, test_tpr)
            self.addtolegend(graphs[f"Train_{key}"], f"{key} (Train) AUC={train_auc:.3f}", "L")
            self.addtolegend(graphs[f"Test_{key}"], f"{key} (Test) AUC={test_auc:.3f}", "L")
            graphs[f"Train_{key}"].SetLineColor(kBlue + train_fold)
            graphs[f"Test_{key}"].SetLineColor(kRed + test_fold)
            graphs[f"Train_{key}"].SetLineWidth(2)
            graphs[f"Test_{key}"].SetLineWidth(2)
            train_fold += 1
            test_fold += 1
            mg.Add(graphs[f"Train_{key}"])
            mg.Add(graphs[f"Test_{key}"])
        self.m_hists["mg"] = mg
        self.m_hists["mg"].Draw("A")
        self.m_hists["mg"].SetMaximum(1.5)

    def draw(self):
        """Draw method

        :returns:

        """
        self._drawgraphs()
        self.m_legend.Draw("same")


class permutationimportanceplot(basicplot):
    """ Creates a standard permutation importance plot

    """

    def __init__(self, valdict, sampling, target, vlabels, plotname="Permutation importance plot"):
        super().__init__(plotname)
        self.m_valdict = valdict
        self.m_sampling = sampling
        self.m_target = target
        self.m_vlabels = vlabels
        self.initlegend(.68, .725, .95, .90)
        self._definehist()

    def _calcauc(self):
        """TODO describe function

            :returns:

            """
        meanauclist, stdauclist = [], []
        for vname, valueslist in self.m_valdict.items():
            auctmp = []
            for values in valueslist:
                fpr, tpr, _ = roc_curve(y_true=values[0].values,
                                        y_score=values[1].values,
                                        sampling=self.m_sampling,
                                        target=self.m_target,
                                        weights=values[2].values,
                                        verbose=False)

                if vname == "nominal":
                    aucnom = metrics.auc(fpr, tpr)
                else:
                    auctmp.append(metrics.auc(fpr, tpr))
            if vname != "nominal":
                meanauclist.append(np.mean(auctmp))
                stdauclist.append(np.std(auctmp))
        meanauclist = (aucnom - meanauclist) / aucnom
        stdauclist = stdauclist / aucnom
        idx = sorted(range(len(meanauclist)), key=lambda k: meanauclist[k], reverse=True)
        return meanauclist[idx], stdauclist[idx], np.array(self.m_vlabels)[idx]

    def _definehist(self):
        """TODO describe function

        :returns:

        """
        aucs, stds, vlabels = self._calcauc()
        self.m_hists["h1"] = TH1D("mean", "", int(len(aucs)), 0, len(aucs))
        self.m_hists["h2"] = TH1D("mean+err", "", int(len(aucs)), 0, len(aucs))
        self.m_hists["h3"] = TH1D("mean-err", "", int(len(aucs)), 0, len(aucs))
        self.m_hists["h1"].SetFillColor(4)
        self.m_hists["h1"].SetMarkerSize(0)
        self.m_hists["h1"].SetBarWidth(0.5)
        self.m_hists["h1"].SetBarOffset(0.25)
        self.m_hists["h2"].SetFillColor(1)
        self.m_hists["h2"].SetMarkerSize(0)
        self.m_hists["h2"].SetBarWidth(0.5)
        self.m_hists["h2"].SetBarOffset(0.25)
        self.m_hists["h3"].SetLineColor(0)
        self.m_hists["h3"].SetLineWidth(0)
        self.m_hists["h3"].SetFillColor(4)
        self.m_hists["h3"].SetMarkerSize(0)
        self.m_hists["h3"].SetBarWidth(0.5)
        self.m_hists["h3"].SetBarOffset(0.25)

        self.m_hists["h2"].SetFillStyle(3345)
        bin_ID = 0
        for auc, std, vlabel in zip(aucs, stds, vlabels):
            self.m_hists["h1"].SetBinContent(bin_ID + 1, auc)
            self.m_hists["h2"].SetBinContent(bin_ID + 1, auc + std)
            self.m_hists["h3"].SetBinContent(bin_ID + 1, auc - std)
            self.m_hists["h1"].GetXaxis().SetBinLabel(bin_ID + 1, vlabel.replace(" [GeV]", ""))
            bin_ID += 1
        self.m_hists["h1"].SetMinimum(0)
        self.m_hists["h1"].SetMaximum(aucs[np.argmax(aucs)] + 3 * stds[np.argmax(aucs)])
        self.m_hists["h1"].GetXaxis().SetLabelSize(0.04)
        self.m_hists["h1"].GetYaxis().SetLabelSize(0.04)

    def draw(self):
        """TODO describe function

        :returns:

        """
        gStyle.SetHatchesSpacing(0.5)
        self.m_hists["h1"].Draw("hbar")
        self.m_hists["h2"].Draw("hbar same")
        self.m_hists["h3"].Draw("hbar same")
        self.addtolegend(self.m_hists["h1"], "Importance", "f")
        self.addtolegend(self.m_hists["h2"], "Uncertainty", "f")
        self.m_legend.Draw("same")

class confusionmatrix(basicplot):

    def __init__(self, confmatrix, classes, plotname="Permutation importance plot"):
        super().__init__(plotname)
        self.m_classes = classes
        self.buildhist(confmatrix)

    def buildhist(self, confmatrix):
        """Function to fill 2D histogram (confusion matrix)

        :param confmatrix: 2D numpy matrix
        :returns: 

        """
        confmatrix_hist = TH2D("ConfMat", "", len(self.m_classes), 0, len(self.m_classes), len(self.m_classes), 0, len(self.m_classes))
        for binIDx, _ in enumerate(self.m_classes):
            confmatrix_hist.GetXaxis().SetBinLabel(binIDx + 1, self.m_classes[binIDx])
            confmatrix_hist.GetYaxis().SetBinLabel(binIDx + 1, self.m_classes[binIDx])
            for binIDy in range(len(self.m_classes)):
                confmatrix_hist.SetBinContent(binIDx + 1, binIDy + 1, confmatrix[binIDy][binIDx]*100)
        confmatrix_hist.SetMaximum(100.0)
        confmatrix_hist.SetMinimum(0.0)
        self.addhist("confmatrix", confmatrix_hist)

    def draw(self):
        self.m_hists["confmatrix"].GetXaxis().SetTitle("Prediction")
        self.m_hists["confmatrix"].GetYaxis().SetTitle("True Label")
        self.m_hists["confmatrix"].GetYaxis().SetTitleOffset(2.8)
        self.m_hists["confmatrix"].GetZaxis().SetTitle("Fraction of Events [%]")
        self.m_hists["confmatrix"].SetMarkerSize(2)
        self.m_canvas.SetRightMargin(0.2)
        self.m_canvas.SetLeftMargin(0.25)
        gStyle.SetPaintTextFormat(".2f")
        self.m_hists["confmatrix"].Draw("colz text")

