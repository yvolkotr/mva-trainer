"""
Module to handle setting ATLAS labels.
"""
from ROOT import TLatex, gPad


def ATLASLabel(x, y, text, color=1):
    """
    An ATLAS label
    """
    l = TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.SetTextSize(0.05)
    delx = 0.165 * 550 * gPad.GetWh() / (472 * gPad.GetWw())
    l.DrawLatex(x, y, "ATLAS")
    if text:
        p = TLatex()
        p.SetNDC()
        p.SetTextSize(0.05)
        p.SetTextFont(42)
        p.SetTextColor(color)
        p.DrawLatex(x + delx, y, text)


def CustomLabel(x, y, text, color=1):
    """
    A custom label
    """
    p = TLatex()
    p.SetNDC()
    p.SetTextSize(0.05)
    p.SetTextFont(42)
    p.SetTextColor(color)
    p.DrawLatex(x, y, text)


def DrawLabels(ALabel, CMLabel, MyLabel, Option="data", align="left"):
    """Function to draw labels into plots

    Keyword arguments:
    ALabel  --- ATLAS Label.
    CMLabel --- Centre of Mass label.
    MyLabel --- Additional label to be printed on the plot.
    Option  --- Option to determine whether 'Simulation' is added to the label. Can be 'data' or 'MC'.
    align   --- Can be 'left' or 'right'.
    """
    y_pos = 0.85
    if align == "left":
        x_pos = 0.2
    if align == "left_2":
        x_pos = 0.3
    if align == "left_3":
        x_pos = 0.4
    if align == "right":
        x_pos = 0.6
    if align == "right_2":
        x_pos = 0.5
    if align == "right_3":
        x_pos = 0.4
    if align == "top_outside":
        x_pos = 0.175
        y_pos = 0.95
    if ALabel.lower() != "none":
        if Option == "data":
            Label = ALabel.replace("Simulation", "")
            ATLASLabel(x_pos, y_pos, Label)
        elif Option == "MC" and "Simulation" not in ALabel:
            Label = "Simulation " + ALabel
            ATLASLabel(x_pos, y_pos, Label)
        else:
            ATLASLabel(x_pos, y_pos, ALabel)
    if CMLabel != "" and align != "top_outside":
        CustomLabel(x_pos, y_pos - 0.05, CMLabel)
    if MyLabel != "" and align != "top_outside":
        CustomLabel(x_pos, y_pos - 0.1, MyLabel)
