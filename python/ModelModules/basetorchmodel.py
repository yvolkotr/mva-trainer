"""This module serves as a base model for PyTorch based neural networks.
Any more complicated PyTorch (or Pytorch gemoetric) NN inherits from the
basetorchmodel class in this module.

In addition callbacks such as EarlyStopping should be introduced here.

"""
import torch
import numpy as np

from HelperModules.messagehandler import TrainerMessage
from torch.nn import (ELU, BatchNorm1d, Dropout, LeakyReLU, Linear, ReLU,
                      Sequential, Sigmoid, Softmax, Tanh)


def get_pytorch_act(act, dim=1):
    """Method to get PyTorch activation function based on string

    :param act:
    :returns:

    """
    if act == "sigmoid":
        return Sigmoid()
    if act == "softmax":
        return Softmax(dim=dim)
    if act == "tanh":
        return Tanh()
    if act == "leaky_relu":
        return LeakyReLU()
    if act == "elu":
        return ELU()
    return ReLU()


class modelcheckpoint():
    """
    Class to save the best model while training. If the current epoch's
    validation loss is less than the previous least less, then save the
    model state.
    """

    def __init__(
        self, best_valid_loss=float('inf')
    ):
        self.modelstate = {"epoch": 0,
                           "model": None,
                           "loss": best_valid_loss}

    def __call__(
        self, current_valid_loss,
        epoch, model
    ):
        """Call method

        :param current_valid_loss: Current validation loss
        :param epoch: Current epoch
        :param model: PyTorch model object
        :returns:

        """
        if current_valid_loss < self.modelstate["loss"]:
            self.modelstate["loss"] = current_valid_loss
            self.modelstate["epoch"] = epoch
            self.modelstate["model"] = model

    def __str__(self):
        """custom print function

        :returns: custom string describing the object

        """
        return f"Restoring model {self.modelstate['model']}\n Best epoch: {self.modelstate['epoch']}, best loss: {self.modelstate['loss']}"

    def getbestmodel(self):
        """Getter function to return the best model configuration

        :returns:

        """
        return self.modelstate["model"]


class EarlyStopping():
    """Early Stopping class

    """

    def __init__(self, patience=None, mindelta=None):

        self.patience = patience
        self.mindelta = mindelta
        self.counter = 0
        self.refloss = np.inf
        self.earlystop = False

    def __call__(self, validation_loss):
        if (self.refloss - validation_loss) < self.mindelta:
            self.counter += 1
            if self.counter >= self.patience:
                self.earlystop = True
        else:
            self.counter = 0
            self.refloss = validation_loss

class basetorchmodel(torch.nn.Module):
    """Base class for a PyTorch based NN

    """

    def __init__(self, cfgset):
        super().__init__()
        self.m_cfgset = cfgset
        self.m_lossfct = None
        self.m_device = torch.device('cpu')  # torch.device('cuda' if torch.cuda.is_available() else 'cpu')

        if not self.m_cfgset.get("MODEL").get("MinDelta") is None and not self.m_cfgset.get("MODEL").get("Patience") is None:
            self.m_earlystopping = EarlyStopping(patience=self.m_cfgset.get("MODEL").get("Patience"),
                                                 mindelta=self.m_cfgset.get("MODEL").get("MinDelta"))
        else:
            self.m_earlystopping = None

    def build_fc(self, in_nodes, hidden_nodes, hidden_act, out_nodes=None, out_act=None):
        """Method to create a fully connected (PyTorch) NN.

        :param in_nodes: Number of input nodes.
        :param hidden_nodes: List of integers defining the number of nodes per hidden layer.
        :param hidden_act: List of strings containing identifiers for activation functions in the hidden layers.
        :param out_nodes: Number of output nodes.
        :param out_act: String identifier for output activation function.
        :param input_string: String to define the inputs for Sequential_geometric object.
        :returns: PyTorch Sequential object

        """
        fcmodel = []
        prev_nodes = in_nodes
        layerid = 1
        for nodes, act in zip(hidden_nodes, hidden_act):
            fcmodel.append(Linear(prev_nodes, nodes))
            fcmodel.append(get_pytorch_act(act))
            prev_nodes = nodes
            if layerid in self.m_cfgset.get("MODEL").get("DropoutIndece"):
                fcmodel.append(Dropout(self.m_cfgset.get("MODEL").get("DropoutProb")))
            if layerid in self.m_cfgset.get("MODEL").get("BatchNormIndece"):
                fcmodel.append(BatchNorm1d(nodes))
            layerid += 1
        if out_nodes is not None:
            fcmodel.append(Linear(prev_nodes, out_nodes))
        if out_act is not None:
            fcmodel.append(get_pytorch_act(out_act))
        return Sequential(*fcmodel)

    def setlossfct(self):
        """Setting the loss function based on given string

        :returns:

        """
        if self.m_cfgset.get("MODEL").get("Loss") == "binary_crossentropy":
            self.m_lossfct = torch.nn.BCELoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "categorical_crossentropy":
            self.m_lossfct = torch.nn.CrossEntropyLoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "MSE":
            self.m_lossfct = torch.nn.MSELoss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "MAE":
            self.m_lossfct = torch.nn.L1Loss(reduction='none')
        elif self.m_cfgset.get("MODEL").get("Loss") == "HuberLoss":
            self.m_lossfct = torch.nn.HuberLoss(reduction='none', delta=self.m_cfgset.get("MODEL").get("HuberDelta"))

    def setoptimiser(self, model):
        """Getter method to return the actual optimiser object

        :param model:
        :returns:

        """
        lr = self.m_cfgset.get("MODEL").get("LearningRate")
        optimisers = {"Adadelta": torch.optim.Adadelta(model.parameters(), lr=lr),
                      "Adam": torch.optim.Adam(model.parameters(), lr=lr),
                      "Adamax": torch.optim.Adamax(model.parameters(), lr=lr),
                      "Nadam": torch.optim.NAdam(model.parameters(), lr=lr),
                      "RMSprop": torch.optim.RMSprop(model.parameters(), lr=lr)}
        return optimisers[self.m_cfgset.get("MODEL").get("Optimiser")]

    def get_loss(self, out, targets, weights):
        """Return weighted loss

        :param lossfct: Loss function to be used for loss calculation
        :param out: Output of the model (PyTorch tensor).
        :param data: Batch of graph from which one can extract labels and sample weights.
        :returns: The calculated loss on which backpropagation can be performed.
        :rtype: PyTorch tensor

        """
        if self.m_cfgset.get("MODEL").get("Loss") in ["binary_crossentropy",
                                                      "MSE",
                                                      "MAE",
                                                      "HuberLoss"]:
            loss = self.m_lossfct(out, targets.view(-1, 1))
        else:
            loss = self.m_lossfct(out, targets.flatten().long())
            # loss = self.m_lossfct(out, targets.long())
        loss = torch.div(torch.dot(loss.flatten(), weights.flatten()), torch.sum(weights.flatten()))
        return loss

    def _trainstep(self, model, loader):
        """Function to run training epoch.
        Can be potentially overwritten in child classes such as for gnn models.

        :param model: PyTorch model
        :param loader: PyTorch loader
        :returns: running training loss

        """
        model.train()
        running_loss = 0
        tot = len(loader.dataset)
        for (inputs, targets, weights) in loader:  # Iterate in batches over the training dataset.
            self.m_optimiser.zero_grad()  # zero the parameter gradients.
            inputs.to(self.m_device)  # TO-DO smarter handling of CPU vs GPU usage.
            targets.to(self.m_device)
            weights.to(self.m_device)
            out = model(inputs)  # Perform a single forward pass.
            loss = torch.mul(self.get_loss(out, targets, weights), len(inputs) / tot)
            loss.backward()  # Derive gradients.
            self.m_optimiser.step()  # Update parameters based on gradients.
            running_loss += loss.item()
        return running_loss

    def _teststep(self, model, loader):
        """Function to run testing "epoch".
        Can be potentially overwritten in child classes such as for gnn models.

        :param model: PyTorch model
        :param loader: PyTorch loader
        :returns: running testing loss

        """

        running_loss = 0
        tot = len(loader.dataset)
        for inputs, targets, weights in loader:  # Iterate in batches over the validation dataset.
            inputs.to(self.m_device)  # TO-DO smarter handling of CPU vs GPU usage.
            targets.to(self.m_device)
            weights.to(self.m_device)
            out = model(inputs)  # Perform a single forward pass.
            loss = torch.mul(self.get_loss(out, targets, weights), len(inputs) / tot)
            running_loss += loss.item()
        return running_loss

    def forward(self, data):
        """PyTorch forward pass method.
        The method 'forward' is abstract in 'torch.nn.modules.module' hence we have to override it here.
        It is further overriden by the child classes inheriting from basetorchmodel later on.

        :param data: PyTorch data object
        :returns: PyTorch data object (overriden with model output later)

        """
        return data

    def runepochs(self, model, train_loader, val_loader):
        """Wrapper method to run training over epochs.

        :param model: PyTorch model
        :param train_loader: DataLoader object holding train data
        :param val_loader: DataLoader object holding validation data
        :returns:

        """
        trainloss, valloss = [], []
        mcpt = modelcheckpoint()
        model.to(self.m_device)
        for epoch in range(1, self.m_cfgset.get("MODEL").get("Epochs")):
            trainloss.append(float(self._trainstep(model=model, loader=train_loader)))
            valloss.append(float(self._teststep(model=model, loader=val_loader)))
            mcpt(valloss[-1], epoch, model)
            TrainerMessage(f'Epoch: {epoch:03d}, Train Loss: {trainloss[-1]:.4f}, Val Loss: {valloss[-1]:.4f}')
            if self.m_earlystopping is not None:
                self.m_earlystopping(valloss[-1])
                if self.m_earlystopping.earlystop:
                    break
        if self.m_cfgset.get("MODEL").get("RestoreBest"):
            TrainerMessage(f"Restoring best model configuration: {mcpt}")
            model = mcpt.getbestmodel()
        return model, {"loss": trainloss,
                       "val_loss": valloss}
