# Virtual environment on Lxplus

In the following we will discuss how to setup a python3 virtual environment (*venv*) on lxplus to use `mva-trainer`.
The setup should work ony any system with access to `cvmfs`.

## Initial setup

First we have to setup the typical ATLAS environment:
```
setupATLAS
```
Now we setup `python3` and `ROOT` versions.
This is done in one single command:
```
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
```
Next we instantiate our venv called `myenv` and *start* it:
```
python3 -m venv myenv
source myenv/bin/activate
```

Next we need to reset the `PYTHONPATH` to point at the `PYTHONPATH` in the virtual environment.
Additionally we add the `ROOT` libraries to it.
```
export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib
```
The next step consists of installing and upgrading `pip`
```
python -m ensurepip --upgrade
```
Please verify that `pip` is installed and working.
Now we continue installing the required packages in the environment:
```
pip install --no-cache-dir skl2onnx # will also install scikit-learn
pip install --no-cache-dir uproot
pip install --no-cache-dir pandas
pip install --no-cache-dir tables
pip install --no-cache-dir torch
pip install --no-cache-dir torchvision torchaudio
pip install --no-cache-dir torch_geometric
pip install --no-cache-dir pydot
pip install --no-cache-dir matplotlib
```

Now we check if we can access `ROOT`.
```
root --version
```
You should now see a `ROOT` command prompt showing version 6.26.08.

Now you can use this environment to run the scripts.

If you want to leave the venv do:
```
deactivate
```

## Reactivating the virtual environment

Python paths are crucial elements that determine how the Python interpreter locates and imports modules and packages. A path is essentially a list of directories where Python searches for modules when they are imported in a script or interactive session.

You might have noticed that we have played with these paths earlier when setting up the virtual environment. When you log out of lxplus your environmental variables are "lost". This includes the `PYTHONPATH` variable. We hence have to make sure we set them up again.

For reactivating the virtual environment after reconnecting to lxplus you have to do the following:

```bash
setupATLAS
lsetup "root 6.26.08-x86_64-centos7-gcc11-opt"
```
and then activate the virtual environment (in this case `myenv`):
```
source myenv/bin/activate
```
followed by the setting of the `PYTHONPATH` variable:
```
export PYTHONPATH=$VIRTUAL_ENV/lib64/python3.9/:$ROOTSYS/lib
```
Afterwards you can again use the virtual environment.
