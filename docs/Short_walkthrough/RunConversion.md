# Run a conversion

The `conversion` usually is the first step you have to do. The purpose of the conversion is to convert `.root` files into `.hdf5` (.h5) files which can be easily handled by subsequent python libraries, especially PyTorch.
During this conversion step a slimmed down version of the information available in your ntuples is produced in the `.hdf5` format.

## Setting up a config file

Before you start converting files (or with any other script), you need to define a config file.
In the [config directory](configs) you find some example configs that are run in the git pipeline.
They all start with a `CI` prefix and can serve as a starting point for your own config file.

Open `CI_test_binary_classification.cfg`.
The config file consists of several blocks.
The `GENERAL` block followed by several `VARIABLE` and `SAMPLE` blocks and finally the `OUTPUT` and (DNN)`MODEL` block.
The `GENERAL` block holds general config options such as the (general) `Selection` and the (general) `MCWeight` definitions.
Since the code uses *uproot* you need to pass logic strings in a python-conform way, i.e.:
```
(A)&(B)|(C)
```
instead of the usual c++ way:
```
(A)&&(B)||(C)
```
This means you have to split your logic strings into the smallest inseparable elements **and** change the `&&` and `||` operators to `&`and `|`.
Furthermore, you need to convert additional c++ operators like `!A` like `A==0`.

Below the `GENERAL` block you will find `VARIABLE` blocks of the form

```
VARIABLE
	Name = Variable_1
	Label = "Var 1"
	Binning = 25,-4,9
```

Here you define your variables that you will need in your model.
You need to define one block per variable.
If you want to access a vector-like variable, e.g. something like `jet_pt` then you need to give it the following name

```
Name = jet_pt[:,i]
```
where i is the ith entry of the vector. Moreover, variables that are not directly stored in the input tree can be constructed (composite variables), e.g. the sum of the transverse momentum of the leptons

```
Name = lep_pt_0 + lep_pt_1 + lep_pt_2
```
To construct composite variables by making operations with booleans, one has to play the trick of multiplying each boolean by a number, e.g.

```
VARIABLE
	Name = 1*(lep_ID_0==11) + 1*(lep_ID_1==11) + 1*(lep_ID_2==11)
	Label = "Number of electrons in the event"
	Binning = 4,-0.5,3.5
```

You will also see that one `OUTPUT` is defined.
This block is similar to the `VARIABLE` block but it defines an output.
You need one block for each of your outputs of your mva-algorithm.

## Performing the conversion

From the main directory you can then do:
```sh
python3 python/mva-trainer.py -c <config_path> --convert
```
Hereby `<config_path>` specifies the path to your config file and `--convert` tells the code to run the conversion.
Upon running the script a new directory is created based on the 'Job' option declared in the given config file.
The directory structure looks like this:
```
<JobName>
    ├── Configs/
    ├── Data/
    ├── Model/
    └── Plots/
```
`Configs` will contain copies of all used config files. `Data` will contain dataframes stored in `.h5` format. `Model` will contain all model related files and `Plots` will contain all control plots (and tables).
Check out the command line options via the `-h` command for more information on additional command line arguments.
If you want to e.g. restrict yourself to only a subset of plots or if you want to reproduce dedicated plots you can use the `--plots` command line argument to only produce these plots.
By default all plots are created unless specified otherwise.
If you only want to produce plots without running the conversion again, e.g. if you only want to change the binning in some of your histograms, then you can append the `--plots-only` config option.
