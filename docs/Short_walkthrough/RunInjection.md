### Injecting model outputs using ONNX models

[ONNX](https://onnx.ai/), short for Open Neural Network Exchange, is an open-source format for representing deep learning models. ONNX models allow for interoperability across different deep learning frameworks, making it possible to use models trained in one framework in another for inference.

Using a trained model in multiple frameworks can be a significant advantage for developers, as it allows them to use the framework best suited to their needs for a given task. Additionally, ONNX models can be optimized for performance on specific hardware, such as CPUs, GPUs, or specialized accelerators, allowing for efficient and fast inference.

Overall, using ONNX models for model inference can simplify the deployment process and improve the efficiency and speed of inference, making it a valuable tool for developers in the deep learning community.

In `mva-trainer` you have the option to set `GetONNXModel` to True (default) to receive an `.onnx` in your `Model` directory. You can also specify the `ONNXVersion` which is used for the export (of neural networks).

The inference of models trained with `mva-trainer` and converted into the `.onnx` format is done through the potent [ONNXRuntime](https://onnxruntime.ai/) environment.

ONNXRuntime is a high-performance engine for executing ONNX models. It is an open-source, cross-platform runtime designed to optimize the inference of machine learning models across various hardware and software configurations, including CPUs, GPUs, and specialized accelerators.

The API provides several benefits to developers, including fast and efficient execution of models, support for a wide range of hardware and software configurations, and the ability to run models in a variety of programming languages, including Python, C++, and C#. Additionally, it offers flexible deployment options, including local deployment and cloud-based deployment using Azure or AWS.

ONNXRuntime comes equipped with a range of optimization techniques that it applies to the ONNX graph by default. These include combining nodes where applicable and extracting constant values through a process called constant folding. Additionally, ONNXRuntime is capable of collaborating with various hardware accelerators via its *Execution Provider interface*. The supported hardware platforms include CUDA, TensorRT, OpenVINO, CoreML, and NNAPI, and are dependent on the target hardware.
To get started with ONNXRuntime see [here](https://onnxruntime.ai/docs/get-started/).

In ATLAS ONNXRuntime is part of the `atlasexternals` packages. For more information on how ONNXRuntime is used in ATLAS code see [here](https://gitlab.cern.ch/atlas/atlasexternals/-/tree/master/External/onnxruntime).

Below you can find an example block on how to run `onnx runtime` in c++ following the ONNXRuntime documentation (might differ from ATLAS implementation):

```
  // Include the onnx api headers for onnx runtime
  #include <onnxruntime_cxx_api.h>
  
  // Allocate ONNXRuntime session
  auto memory_info = Ort::MemoryInfo::CreateCpu(OrtDeviceAllocator, OrtMemTypeCPU);
  Ort::Env env;
  Ort::Session session{env, L"model.onnx", Ort::SessionOptions{nullptr}};

  // Allocate model inputs: fill in shape and size
  std::array<float, ...> input{};
  std::array<int64_t, ...> input_shape{...};
  Ort::Value input_tensor = Ort::Value::CreateTensor<float>(memory_info, input.data(), input.size(), input_shape.data(), input_shape.size());
  const char* input_names[] = {...};

  // Allocate model outputs: fill in shape and size
  std::array<float, ...> output{};
  std::array<int64_t, ...> output_shape{...};
  Ort::Value output_tensor = Ort::Value::CreateTensor<float>(memory_info, output.data(), output.size(), output_shape.data(), output_shape.size());
  const char* output_names[] = {...};

  // Run the model
  session_.Run(Ort::RunOptions{nullptr}, input_names, &input_tensor, 1, output_names, &output_tensor, 1);
```

### Run an injection
If you want to work directly with your MVA outputs you should "re-inject" them into your Ntuples.
The idea of this process is that the MVA outputs are added as additional branches to the Ntuples. During this process a copy of the Ntuples is produced, including the newly added branches.

To inject NN predictions back into Ntuples you can do:
```sh
python3 python/mva-trainer.py -i <input_path> -o <output_path> -c <config_path> --inject --processes <n_processes> --filter <filter_string>
```
Hereby `<input_path>` describes the path to input Ntuples. The `<output_path>` argument describes the path to an output directory in which the injected Ntuples will be stored. The directory and the sub-directory structure will automatically be created for you. The config file is **again** passed via the `<config_path>` argument.
You can pass multiple config files simultaneously inject multiple neural networks into Ntuples using the `--additionalconfigs` argument.
Using the *processes* argument you can specify a number of independent processes that are run simultaneously. Each process will inject the prediction into one root file and start a new process upon termination. Using the `filter` argument you can restrict your injection using a string as a wildcard.
Keep in mind that this can be very memory-consuming because the entire root-file is loaded. Restrict this to a lower number (e.g. 2-4) in case you have memory limitations.
