# Setting up mva-trainer and Prerequisities

To obtain the code simply clone the repository using a protocol of your choice:
```sh
git clone ssh://git@gitlab.cern.ch:7999/skorn/mva-trainer.git
```
If this is your first time cloning the repository you need to verify that you have the necessary python modules installed.
For this purpose run the `setup.sh` script via:
```sh
source setup.sh
```
The script will check whether the necessary packages exist. If packages are missing an error message will be printed.
Please install any missing packages.

Each time you start a new shell you need to run the setup script again. It will set environmental variables that are picked up by the code.
The code itself is python based and hence does not need any compilation.

# Package dependencies

The code should work using these package versions:

| **Package** | **Version** |
| ------ | ------ |
| Python | 3.8 or later|
| uproot | 5.02 or later
| pandas | 1.5.2 or later |
| scikit-learn | 1.2.0 |
| skl2onnx | 1.14.0 or later |
| tables | 3.8.0 or later |
| matplotlib | 3.6.2 or later |
| pydot | 1.4.2 or later |
| torch | 1.12.1 or later |

The following lines should install the necessary prerequisities:
```sh
python3 -m pip install --no-cache-dir --upgrade uproot==5.0.2
python3 -m pip install --no-cache-dir --upgrade pandas==1.5.2
python3 -m pip install --no-cache-dir --upgrade scikit-learn==1.2.0
python3 -m pip install --no-cache-dir --upgrade tables==3.8.0
python3 -m pip install --no-cache-dir --upgrade matplotlib==3.6.2
python3 -m pip install --no-cache-dir --upgrade pydot==1.4.2
python3 -m pip install --no-cache-dir --upgrade skl2onnx
python3 -m pip install --no-cache-dir torch==1.12.1 torchvision torchaudio
python3 -m pip install --no-cache-dir torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric
```

Please raise an issue if you find versions that are leading to conflicts between packages.