# Graph neural networks

Graph Neural Networks (GNNs) are machine learning models that can operate on data represented as graphs. Graphs consist of nodes (vertices) and edges (connections between nodes), and GNNs aim to learn meaningful representations of nodes and edges that capture the underlying patterns and relationships in the graph structure.
One example of a real-life application of GNNs is in social network analysis. Social networks can be modelled as graphs, with individuals represented as nodes and their relationships represented as edges. GNNs can be used to learn representations of individuals that capture their social connections and enable tasks such as predicting who is likely to form new connections or identifying groups with similar social behaviours.
Another example of a real-life application of GNNs is drug discovery. Molecules can be represented as graphs, with atoms represented as nodes and chemical bonds represented as edges. GNNs can be used to learn representations of molecules that capture their structural and functional properties, enabling tasks such as predicting their biological activity or designing new molecules with desired properties.
In particle physics, particle collisions can be represented as complex graphs, with particles as nodes and their interactions as edges. GNNs can be used to learn representations of particles and their interactions that capture the underlying physics and enable tasks such as identifying rare events or discovering new particles.
In MVA-trainer graph neural networks are realised using the [PyTorch Geometric library](https://pytorch-geometric.readthedocs.io/en/latest/).
PyTorch Geometric is an open-source library for handling geometric deep-learning operations on graphs, meshes, and point clouds. It provides a set of standard datasets, neural network layers, and utility functions for deep learning on irregular, graph-structured data.
PyTorch Geometric extends [PyTorch](https://pytorch.org/) with modules and classes that allow users to easily manipulate and transform graph-structured data. It provides a unified framework for building and training graph neural networks (GNNs), which can operate on data represented as graphs.
One of the main advantages of PyTorch Geometric is that it provides a high-level abstraction for handling graph data, simplifying the development of GNN models. It provides efficient implementations of common graph operations, such as graph convolutions, pooling, and message passing, which are vital building blocks of GNNs.

The building of graphs happens automatically in the MVA-trainer.
Just like for DNNs it is based on information passed via the config files.
Depending on the type of the graph and the corresponding network the necessary options differ slightly.

## Homogeneous and heterogeneous graphs

Heterogeneous and homogeneous graphs are two types of graph structures that differ in their node and edge types.
A homogeneous graph is a graph in which all nodes are of the same type, and all edges connect nodes of the same type. For example, a social network graph where all nodes represent users and all edges represent relationships between users is a homogeneous graph.

A heterogeneous graph, on the other hand, is a graph in which nodes can belong to different types and edges can connect nodes of different types. For example, a knowledge graph where nodes represent entities such as people, places, and organizations, and edges represent different types of relations between them (such as "worked at", "lives in", and "studied at") is a heterogeneous graph.

The main difference between homogeneous and heterogeneous graphs is the diversity of node and edge types. Homogeneous graphs are more limited in their ability to represent complex relationships between different types of entities, while heterogeneous graphs can represent richer and more complex structures.

Another essential difference between homogeneous and heterogeneous graphs is how they are modelled and analyqed. Homogeneous graphs can be analyzed using standard algorithms and techniques like graph clustering and centrality measures. Heterogeneous graphs, however, require specialiwed algorithms and techniques, such as meta-path-based methods and attention mechanisms, considering the diversity of node and edge types.

In the following the two main types of graphs, homogeneous and heterogeneous, and how they are created in mva-trainer, are highlighted a bit more.
Afterwards the structure of GNN models as implemented in mva-trainer is explained.
In general, graphs can be homogeneous or heterogeneous.
They represent networks with nodes and edges connecting the individual nodes.
Fundamentally homogeneous and heterogeneous graphs differ in how these nodes and edges are defined.

## Homogeneous graphs

A homogeneous graph (in machine learning) consists of nodes (black dots in the figure) and edges of identical dimensions (solid blue lines).
This means that each node in the graph has precisely `F` features, and each edge has exactly `E` edge features.
All connections are represented via a square adjacency matrix which determines whether there is a connection (edge) between any two nodes of the graph:

![Simple graph](../img/Graph_01.png)

The power of a GNN is that its input (i.e.) the graph can technically have any size.
This means that the graph could have 3, 5, 10 or 100 nodes, and the GNN could still work with it.
In HEP, the nodes often represent (e.g.) reconstructed objects in the detector.
For this example, we will assume that these are jets and their reconstructed properties.
Naturally, every jet has the same properties; hence, the feature vector `F` for every jet has the same dimension.
Furthermore, we can assign edge attributes; for example, the distance between these jets in the detector is measured in eta and phi.
These would then be the edge attributes.

Technically this is done in the following way (here with three jets):
```
GRAPHNODE
        Name = "Jet_1"
        Type = "Jet"
        Features = "Jet_1_pT", "Jet_1_eta", "Jet_1_phi"
        Targets = "Jet_1", "Jet_3"
        EdgeFeatures = "Jet_1_2_deta","Jet_1_2_dphi"|"Jet_1_3_deta","Jet_1_3_dphi"
        
GRAPHNODE
        Name = "Jet_2"
        Type = "Jet"
        Features = "Jet_2_pT", "Jet_2_eta", "Jet_2_phi"
        Targets = "Jet_1", "Jet_3"
        EdgeFeatures = "Jet_1_2_deta","Jet_2_3_dphi"|"Jet_2_3_deta","Jet_2_3_dphi"

GRAPHNODE
        Name = "Jet_3"
        Type = "Jet"
        Features = "Jet_3_pT", "Jet_3_eta", "Jet_3_phi"
        Targets = "Jet_1", "Jet_2"
        EdgeFeatures = "Jet_1_3_deta","Jet_1_3_dphi"|"Jet_2_3_deta","Jet_2_3_dphi"
```

Each `GRAPHNODE` object defines one new node.
The `type` will be necessary once we discuss *heterogeneous* jets.
The `Features` represent the node's features (`F`); the targets are all the other `GRAPHNODE`'s this node connects to.
Finally, the `EdgeFeatures` is a list of features per edge in the same order as the targets, separated by `|`. I.e. the first list represents the edge features connecting the particular `GraphNode` with its first target.

What if there are fewer objects than I defined in the config file?
In general, graphs should be defined in this framework by defining the maximum possible graph.
I.e. if you want to train up to 10 jets in the example above, you should define 10 `GRAPHNODE` objects with the respective connections.
If an event has less than ten jets, use the `PruneIfValue` option in the `GRAPHNODE` block and pass a list of values with the same length as `F`.
The code will then check if any of the values in the feature vector of this node has this value (e.g. if you assign -1 to values which are not filled) and subsequently prune the node and remove its edges (white node and dashed blue lines):

![Missing Node](../img/Graph_missing.png)

This way, you do not have to worry about the size of the graph, and let us face it, who trusts ten jet events?

## Heterogeneous graphs

Heterogeneous graphs are a more general form of homogeneous graphs because there can be different types of nodes and edges; they no longer need to have the same feature and edge dimensions.
Instead, the graph is "split" into nodes and edges of different types:

![Missing Node](../img/Graph_02.png)

In this example, we look at two different types of nodes (black and green)
These could now be jets and leptons with different properties.
Consequently, there are now three different types of connections:

* lepton - jet
* lepton - lepton
* jet-jet

All of these edges can also have different properties.
Technically this is realised via the `type` option in each `GRAPHNODE` block.
Via this option, the type of each node is defined, and identical types are grouped.
Hence any form of an event for which one could draw a Feynman diagram can be defined.
Again the pruning mechanism works the same way as in the homogeneous example.

## Global graph features

Graphs might also include global features besides the node and edge features.
In the example above, these would describe the entire event.
To incorporate these types of variables into the graph, a dedicated `GRAPHNODE` block can be used:

```
GRAPHNODE
        Name = "Global Features"
        Type = "Global"
        Features = "variable_1","variable_2","variable_3"
```

Here the reserved `Global` option is used as a `type`.
Nodes defined as global are used to build separate, densely connected networks.
Their architecture is steered via the `GlobalNodes` and `GlobalActivationFunctions` options.
Their output is then concatenated together with the output of the graph convolutional layers.

## Structure of a full GNN in MVA-trainer

Graph neural networks (GNNs) in MVA-trainer consist of three parts:

* a graph convolution model
* a global model
* a fully connected model

The graph convolution model takes the graph data and applies user-defined convolutions.
The global model takes global data and feeds it through a dense neural network.
The fully connected model (here, also called the final model) then combines the interim outputs of these two models.
All three are combined into one GNN model, which is then trained:

![Simple graph](../img/GNN_Model.png)
