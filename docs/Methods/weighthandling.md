## Handling of Monte Carlo weights

Event weights, i.e. Monte Carlo (MC) weights, are a way to adjust the contribution of individual events to the training. 

When using event weights, the loss function is modified to consider the weights assigned to each event. The weighted loss is calculated as the average of the per-sample losses multiplied by their corresponding weights. This means that samples with higher weights contribute more to the loss and therefore have a more significant impact on the training of the network.

The formula for the weighted loss can be written as:

<img src="https://latex.codecogs.com/svg.image?L(w)&space;=&space;\frac{1}{N}&space;\sum_{i=1}^{N}w_i&space;\cdot&space;L_i" title="Weighted loss function" />

where `N` is the total number of samples in the training set, `w_i` is the weight assigned to the i-th sample, and `L_i` is the per-sample loss. The weighted loss, `L(w)`, is the average of the per-sample losses, weighted by their corresponding weights.

## Handling of imbalanced datasets

Imbalanced datasets occur when there is a significant disparity in the number of events per class in a dataset. This can lead to biased models that perform poorly on minority classes, as they are not exposed to enough examples during training.

To address this, scale factors can be introduced to normalise the sum of event weights per class. The scale factors are calculated by dividing the total number of events by the sum of the event weights for each class. The scale factors are then applied to the event weights to ensure that the integral of all MC event weights per class is identical.

The formula for calculating the scale factors, `S`, is:

<img src="https://latex.codecogs.com/svg.image?S_j=\frac{1}{\sum w_i^\text{Class j}}" title="scale_factor" />

where `w_i` is the event weight for the i-th event of the j-th class.

The normalised event weight, `w_i`, can then be calculated for each class as:

<img src="https://latex.codecogs.com/svg.image?w_i^\text{Class j}{}'=w_i^\text{Class j}\cdot S_j" title="normalised weights" />

By normalising the event weights using scale factors, the model is less likely to be biased towards certain classes and can learn to distinguish between minority and majority classes.

In `mva-trainer` this setting is enabled through the `WeightScaling` option in the config file. The argument Can be set to `True` of `False`. If it is set to `True` (default), weights are scaled to normalise the sum over all labels.

## Handling of negative event weights

In some cases, event weights may be negative, which can cause issues during training. To address this, one approach is to set negative event weights to zero before training. However, this can lead to a biased model, as the total weight of the remaining positive event weights will need to be corrected.

A sample-wise correction factor can be calculated and applied to the positive event weights to address this issue. The correction factor is based on the ratio of the sum of the positive event weights to the sum of all event weights (including negative weights) and accounts for the negative event weights that are being set to zero.

The formula for calculating the correction factor, `C` is:

<img src="https://latex.codecogs.com/svg.image?C=\frac{\sum_{w_i\geq0}w_i}{\sum_{i}w_i}" title="correction_factor" />

where `w_i` is the event weight for the i-th event, and the sum is taken over all events in the dataset.

The corrected event weight, `w_i'`, can then be calculated as:

<img src="https://latex.codecogs.com/svg.image?w_i'=w_i\cdot C\cdot[w_i>0]" title="corrected weight" />

where the bracket notation denotes that the term inside is only evaluated when `w_i` is greater than zero. All other event weights set to zero.

This formula ensures that the positive event weights are scaled to account for the negative event weights set to zero, and that the remaining positive event weights are correct.

Alternative options are only to use the positive event weights, their absolute value or simply all event weights. The latter option should always be tested first as it best reflects the physics.
All these options can be steered through the `NegWeightTreatment` option in the config file.
It can be set to `Scale`, which removes negative weights and scales remaining weights down to account for the difference, to `SetZero`, which sets all negative weights to zero or to `None`, where negative weights stay untouched. Users are encouraged to propose different weighting schemes, especially checking which one is adequate for their case. If the `Scale` option is chosen, comparison plots are created with both the scaled and non-negative weight distributions.
