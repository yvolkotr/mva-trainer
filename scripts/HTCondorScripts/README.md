### Setup

Sometimes it might be desirable to run things using HTCondor. The following scripts were tested on lxplus and should work there, making your life a little easier.

The aim of these scripts is to simply allow for the `Converter.py`, `Trainer.py` and `Evaluate.py` scripts to be send to HTCondor.
To make this work you have to set up a few things:


All of this should be run outside any containerised environment. However, there the `setup.sh` script will likely not work properly. However, this is not a problem. We only need one environmental variable from it.
Type:
```
export MVA_TRAINER_BASE_DIR="path/to/mva-trainer-repo"
```
where `path/to/mva-trainer-repo` is the absolute path to the repo.
Afterwards open `scripts/HTCondorScripts/MVA.sub` using your favourite editor.
In `scripts/HTCondorScripts/MVA.sub` you can define the structure of your HTCondor job.
The most important line for you is:
```
arguments               = $ENV(MVA_TRAINER_BASE_DIR)/Singularity-Image.sif Converter $ENV(MVA_TRAINER_BASE_DIR)/config/ExampleConfig.cfg
```
The first argument is the absolute path to your singularity image, the second one is a string (it can be `Converter`, `Trainer`, `Evaulater` or `All`, depending on what script you want to run) and the last part is the absolute path to your config file.
The directory of the job as you know it from the repo will later be created inside the main directory of the repo.
After this type:
```
condor_submit scripts/HTCondorScripts/MVA.sub
```
from the main directory to submit your job to the HTCondor queue.
