"""
Module to create dummy ntuples for CI
"""

import os
import uproot
import numpy as np

print("CI PREPARATION:\t creating Ntuple directory")
os.makedirs("CI_Test_Ntuples", exist_ok=True)
print("CI PREPARATION:\t Settind numpy seed to 1")
np.random.seed(1)

nEvents = 100000


def get_pseudophi(size=100000):
    """Function to produce pseudo phi distribution

    :param size: number of entries in distribution (default 100,000)
    :returns:

    """
    return np.random.uniform(low=-3.141, high=3.141, size=size)


def get_pseudoeta(size=100000, scale=3.0):
    """Function to produce a pseudo eta distribution

    :param size: number of entries in distribution (default 100,000)
    :param scale: width of the distribution using a normal distribution
    :returns:

    """
    return np.random.normal(loc=0, scale=scale, size=size)


def get_pseudopT(size=100000, mean=40, sigma=1):
    """Function to get a pseudo pT distribution

    :param size: number of entries in distribution (default 100,000)
    :param mean: mean of the distribution using a lognormal distribution
    :param sigma: width of the distribution using a lognormal distribution
    :returns:

    """
    return np.random.lognormal(mean=mean, sigma=sigma, size=size)


def get_pseudobtagging(loc=2, scale=1, size=100000):
    """Function to get a pseudo b-tagging score distribution

    :param loc: mean of the distribution using a normal distribution
    :param scale: width of the distribution using a normal distribution
    :param size: number of entries in the distribution (default 100,000)
    :returns:

    """
    scores = np.random.normal(loc=loc, scale=scale, size=size).astype(int)
    scores[scores < 1] = 1
    scores[scores > 5] = 5
    return scores


def get_pseudodR(phi_1, phi_2, eta_1, eta_2):
    """Function to calculate delta R using phi and eta values

    :param phi_1: first phi value
    :param phi_2: second phi value
    :param eta_1: first eta value
    :param eta_2: second eta value
    :returns:

    """
    return np.sqrt((phi_1 - phi_2)**2 + (eta_1 - eta_2)**2)


################
### Sample A ###
################
print("CI PREPARATION:\t creating sample_a.root")
with uproot.recreate("CI_Test_Ntuples/sample_a.root") as f_a:
    var_dict = {"Object_1_pT": get_pseudopT(size=nEvents, mean=1.8, sigma=1),
                "Object_1_phi": get_pseudophi(size=nEvents),
                "Object_1_eta": get_pseudoeta(size=nEvents, scale=1),
                "Object_1_bTagWP": get_pseudobtagging(size=nEvents, loc=1),
                "Object_2_pT": get_pseudopT(size=nEvents, mean=2.8, sigma=1),
                "Object_2_phi": get_pseudophi(size=nEvents),
                "Object_2_eta": get_pseudoeta(size=nEvents, scale=1),
                "Object_2_bTagWP": get_pseudobtagging(size=nEvents, loc=1),
                "Object_3_pT": get_pseudopT(size=nEvents, mean=3.8, sigma=1),
                "Object_3_phi": get_pseudophi(size=nEvents),
                "Object_3_eta": get_pseudoeta(size=nEvents, scale=1),
                "Object_3_bTagWP": get_pseudobtagging(size=nEvents, loc=5),
                "weight_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "weight_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "eventNumber": np.arange(nEvents)}
    var_dict["Object_1_2_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_2_phi"], var_dict["Object_1_eta"], var_dict["Object_2_eta"])
    var_dict["Object_2_3_dR"] = get_pseudodR(var_dict["Object_2_phi"], var_dict["Object_3_phi"], var_dict["Object_2_eta"], var_dict["Object_3_eta"])
    var_dict["Object_1_3_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_3_phi"], var_dict["Object_1_eta"], var_dict["Object_3_eta"])
    var_dict["Truth_Variable"] = var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10
    f_a["test_tree"] = var_dict
    f_a["truth"] = {"Truth_Variable": var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10,
                    "MC_Object_1_pT": var_dict["Object_1_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_1_phi": var_dict["Object_1_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_1_eta": var_dict["Object_1_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_2_pT": var_dict["Object_2_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_2_phi": var_dict["Object_2_phi"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_2_eta": var_dict["Object_2_eta"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_3_pT": var_dict["Object_3_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_3_phi": var_dict["Object_3_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_3_eta": var_dict["Object_3_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "eventNumber": np.arange(nEvents)}
f_a.close()
################
### Sample B ###
################
print("CI PREPARATION:\t creating sample_b.root")
with uproot.recreate("CI_Test_Ntuples/sample_b.root") as f_b:
    var_dict = {"Object_1_pT": get_pseudopT(size=nEvents, mean=2, sigma=1),
                "Object_1_phi": get_pseudophi(size=nEvents),
                "Object_1_eta": get_pseudoeta(size=nEvents, scale=1.25),
                "Object_1_bTagWP": get_pseudobtagging(size=nEvents, loc=2),
                "Object_2_pT": get_pseudopT(size=nEvents, mean=3, sigma=1),
                "Object_2_phi": get_pseudophi(size=nEvents),
                "Object_2_eta": get_pseudoeta(size=nEvents, scale=1.25),
                "Object_2_bTagWP": get_pseudobtagging(size=nEvents, loc=2),
                "Object_3_pT": get_pseudopT(size=nEvents, mean=4, sigma=1),
                "Object_3_phi": get_pseudophi(size=nEvents),
                "Object_3_eta": get_pseudoeta(size=nEvents, scale=1.25),
                "Object_3_bTagWP": get_pseudobtagging(size=nEvents, loc=5),
                "weight_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "weight_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "eventNumber": np.arange(nEvents)}
    var_dict["Object_1_2_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_2_phi"], var_dict["Object_1_eta"], var_dict["Object_2_eta"])
    var_dict["Object_2_3_dR"] = get_pseudodR(var_dict["Object_2_phi"], var_dict["Object_3_phi"], var_dict["Object_2_eta"], var_dict["Object_3_eta"])
    var_dict["Object_1_3_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_3_phi"], var_dict["Object_1_eta"], var_dict["Object_3_eta"])
    var_dict["Truth_Variable"] = var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10
    f_b["test_tree"] = var_dict
    f_b["truth"] = {"Truth_Variable": var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10,
                    "MC_Object_1_pT": var_dict["Object_1_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_1_phi": var_dict["Object_1_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_1_eta": var_dict["Object_1_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_2_pT": var_dict["Object_2_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_2_phi": var_dict["Object_2_phi"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_2_eta": var_dict["Object_2_eta"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_3_pT": var_dict["Object_3_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_3_phi": var_dict["Object_3_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_3_eta": var_dict["Object_3_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "eventNumber": np.arange(nEvents)}
f_b.close()

################
### Sample C ###
################
print("CI PREPARATION:\t creating sample_c.root")
with uproot.recreate("CI_Test_Ntuples/sample_c.root") as f_c:
    var_dict = {"Object_1_pT": get_pseudopT(size=nEvents, mean=2.2, sigma=1),
                "Object_1_phi": get_pseudophi(size=nEvents),
                "Object_1_eta": get_pseudoeta(size=nEvents, scale=1.5),
                "Object_1_bTagWP": get_pseudobtagging(size=nEvents, loc=2.5),
                "Object_2_pT": get_pseudopT(size=nEvents, mean=3.2, sigma=1),
                "Object_2_phi": get_pseudophi(size=nEvents),
                "Object_2_eta": get_pseudoeta(size=nEvents, scale=1.5),
                "Object_2_bTagWP": get_pseudobtagging(size=nEvents, loc=2.5),
                "Object_3_pT": get_pseudopT(size=nEvents, mean=4.2, sigma=1),
                "Object_3_phi": get_pseudophi(size=nEvents),
                "Object_3_eta": get_pseudoeta(size=nEvents, scale=1.5),
                "Object_3_bTagWP": get_pseudobtagging(size=nEvents, loc=4),
                "weight_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "weight_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_1": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "Selection_2": np.random.normal(loc=1.0, scale=1.0, size=nEvents),
                "eventNumber": np.arange(nEvents)}
    var_dict["Object_1_2_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_2_phi"], var_dict["Object_1_eta"], var_dict["Object_2_eta"])
    var_dict["Object_2_3_dR"] = get_pseudodR(var_dict["Object_2_phi"], var_dict["Object_3_phi"], var_dict["Object_2_eta"], var_dict["Object_3_eta"])
    var_dict["Object_1_3_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_3_phi"], var_dict["Object_1_eta"], var_dict["Object_3_eta"])
    var_dict["Truth_Variable"] = var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10
    f_c["test_tree"] = var_dict
    f_c["truth"] = {"Truth_Variable": var_dict["Object_1_pT"] + var_dict["Object_2_pT"] + var_dict["Object_3_pT"] + 10,
                    "MC_Object_1_pT": var_dict["Object_1_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_1_phi": var_dict["Object_1_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_1_eta": var_dict["Object_1_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_2_pT": var_dict["Object_2_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_2_phi": var_dict["Object_2_phi"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_2_eta": var_dict["Object_2_eta"] + np.random.normal(loc=0.0, scale=0.5, size=nEvents),
                    "MC_Object_3_pT": var_dict["Object_3_pT"] + np.random.normal(loc=0.0, scale=2.5, size=nEvents),
                    "MC_Object_3_phi": var_dict["Object_3_phi"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "MC_Object_3_eta": var_dict["Object_3_eta"] + np.random.normal(loc=0.0, scale=0.2, size=nEvents),
                    "eventNumber": np.arange(nEvents)}
f_c.close()

################
##### Data #####
################
print("CI PREPARATION:\t creating test_data.root")
with uproot.recreate("CI_Test_Ntuples/test_data.root") as f_data:
    var_dict = {"Object_1_pT": np.concatenate((get_pseudopT(size=nEvents, mean=1.8, sigma=1),
                                               get_pseudopT(size=nEvents, mean=2.0, sigma=1),
                                               get_pseudopT(size=nEvents, mean=2.2, sigma=1)), axis=None),
                "Object_1_phi": get_pseudophi(size=nEvents * 3),
                "Object_1_eta": np.concatenate((get_pseudoeta(size=nEvents, scale=1),
                                                get_pseudoeta(size=nEvents, scale=1.25),
                                                get_pseudoeta(size=nEvents, scale=1.5)), axis=None),
                "Object_1_bTagWP": np.concatenate((get_pseudobtagging(size=nEvents, loc=1),
                                                   get_pseudobtagging(size=nEvents, loc=2),
                                                   get_pseudobtagging(size=nEvents, loc=2.5)), axis=None),
                "Object_2_pT": np.concatenate((get_pseudopT(size=nEvents, mean=2.8, sigma=1),
                                               get_pseudopT(size=nEvents, mean=3.0, sigma=1),
                                               get_pseudopT(size=nEvents, mean=3.2, sigma=1)), axis=None),
                "Object_2_phi": get_pseudophi(size=nEvents * 3),
                "Object_2_eta": np.concatenate((get_pseudoeta(size=nEvents, scale=1),
                                                get_pseudoeta(size=nEvents, scale=1.25),
                                                get_pseudoeta(size=nEvents, scale=1.5)), axis=None),
                "Object_2_bTagWP": np.concatenate((get_pseudobtagging(size=nEvents, loc=1),
                                                   get_pseudobtagging(size=nEvents, loc=2),
                                                   get_pseudobtagging(size=nEvents, loc=2.5)), axis=None),
                "Object_3_pT": np.concatenate((get_pseudopT(size=nEvents, mean=3.8, sigma=1),
                                               get_pseudopT(size=nEvents, mean=4.0, sigma=1),
                                               get_pseudopT(size=nEvents, mean=4.2, sigma=1)), axis=None),
                "Object_3_phi": get_pseudophi(size=nEvents * 3),
                "Object_3_eta": np.concatenate((get_pseudoeta(size=nEvents, scale=1),
                                                get_pseudoeta(size=nEvents, scale=1.25),
                                                get_pseudoeta(size=nEvents, scale=1.5)), axis=None),
                "Object_3_bTagWP": np.concatenate((get_pseudobtagging(size=nEvents, loc=5),
                                                   get_pseudobtagging(size=nEvents, loc=5),
                                                   get_pseudobtagging(size=nEvents, loc=4)), axis=None),
                "weight_1": np.random.normal(loc=1.0, scale=0.0, size=3 * nEvents),
                "weight_2": np.random.normal(loc=1.0, scale=0.0, size=3 * nEvents),
                "Selection_1": np.random.normal(loc=1.0, scale=1.0, size=3 * nEvents),
                "Selection_2": np.random.normal(loc=1.0, scale=1.0, size=3 * nEvents),
                "eventNumber": np.arange(3 * nEvents)}
    var_dict["Object_1_2_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_2_phi"], var_dict["Object_1_eta"], var_dict["Object_2_eta"])
    var_dict["Object_2_3_dR"] = get_pseudodR(var_dict["Object_2_phi"], var_dict["Object_3_phi"], var_dict["Object_2_eta"], var_dict["Object_3_eta"])
    var_dict["Object_1_3_dR"] = get_pseudodR(var_dict["Object_1_phi"], var_dict["Object_3_phi"], var_dict["Object_1_eta"], var_dict["Object_3_eta"])
    f_data["test_tree"] = var_dict
f_data.close()
print("CI PREPARATION:\t Done!")
