FROM rootproject/root:latest
RUN useradd --create-home --shell /bin/bash mva_trainer_user
WORKDIR /home/mva_trainer_user
RUN apt update && \
    apt install -y python3-pip && \
    apt install -y pylint && \
    apt install -y graphviz && \
    apt install -y git && \
    apt install -y emacs
RUN pip3 install --no-cache-dir --upgrade uproot==5.0.2 && \
    pip3 install --no-cache-dir --upgrade pandas==1.5.2 && \
    pip3 install --no-cache-dir --upgrade scikit-learn==1.2.0 && \
    pip3 install --no-cache-dir --upgrade tables==3.8.0 && \
    pip3 install --no-cache-dir --upgrade matplotlib==3.6.2 && \
    pip3 install --no-cache-dir --upgrade pydot==1.4.2 && \
    pip3 install --no-cache-dir --upgrade skl2onnx && \
    pip3 install --no-cache-dir torch==1.12.1 torchvision torchaudio && \
    pip3 install --no-cache-dir torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric

USER mva_trainer_user
COPY . .
CMD ["bash"]

