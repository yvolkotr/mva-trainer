# v2.0.9
 * Fixing bug where train distributions are only evaluated on first fold (f00f190)
 * Fixing remaining issues with formulas in weighthandling documentation (e6b6cec)
 * Fixing formulas in weight handling documentation (f516bf5)
 * Adding information about weightscaling to documentation (06efdd1)
 * Implemented Dropout, batchnorm and let HTCondorMain.sh exit with python exit code if error occured (c46ec82)
 * Setting scale and offset to 1.0 and 0.0, respectively (dfc19d1)
 * Adding new sections to documentation to explain used methods (k-folding) (604c6f2)
 * adding pydot and matplotlib to venv setup (20a19b2)
 * Updating the way pip is installed in virtual environment (2e69c6a)
 * Fixing shuffling of inputs for permutation importance and slightly improving permutation plot visuals (355c9a4)
 * Merge branch '46-incorrect-train-versus-test-plots-due-to-incorrect-retrieval-of-values' into 'master'
 * fixing issues with train test plots taking the wrong curves as inputs (d4be882)

# v2.0.8
 * Fixing incorrect retrieval of predictions for ROC curve generation. (9d90602)
 * Documentation fixes (1942db4)
 * Updating pT-scaling formula (0e92df9)
 * Updating scaling documentation and adding layout picture to DNN (9fd1431)
 * Improving help messages (22594e8)
 * Explicitly setting PYTHONPATH in virtual environment and adding ROOT libraries to it (3a24868)
 * Updating creation of venv to not run pip outside venv to fix issue with outdated cvmfs python packages (88afbc0)

# v2.0.7
 * Updating venv documentation and adding skl2onnx to it (941f5d1)
 * Fix reading input variable such as  jet_pT[:,0] in with uproot" (0fbf729)
 * Merge branch '36-reintroduce-permutation-importance-plots' into 'master'
 * Reintroduce permutation importance plots (e514b12)
 * Change the installation of pytorch in VE (bdc83e7)

# v2.0.6
 * Adding the correct sorting to the indeces to match predictions to their x-values (d9b6b0c)
 * Fixing faq images the second (7b9791a)
 * Fixing formula images in FAQ (6ae5b7c)
 * Updating pipeline to only create ntuples when needed (b8e91bc)
 * Extending FAQ with some more information (a3e0af6)
 * Fixing FAQ (5cacf2a)

# v2.0.5
 * Implementing initial version of ONNX support for DNNs (64bcdc1)
 * Adding checks whether certain histograms actually exist before they are (c659dcf)
 * Fixing times 2 offset for BDT log loss (67a2909)
 * Fixing wrong commands in walkthrough for evaluation and injection (d952b76)

# v2.0.4
 * Fixing standard scaling (98962ec)
 * Fixing wrong condition for early stopping (c40bc70)
 * Change np.std/mean(data,dtype=fnp.loat64) to np.float64(np.std/mean(...)) in datascaler (9d2c9e4)
 * Updating documentation of BDTs, DNNs, and GNNs (976631a)
 * Generalising cases when pipeline should run and fixing jobs in pipeline not being triggered correctly. (f510a37)
 * Cleaning up the saving of dictionaries to json files by introducing indentation (400c592)
 * Removing info messages from scaler (9d2cc4f)
 * Adding flag to only run conversion (acfd5fa)
 * Forcing pT-scaling to return double values (ffc0ab8)

# v2.0.3
 * Adding KS and AD tests to train-test plots. Adding user option 'ComparisonTest' (af0890a)
 * HO path_fixes / additions (fde18cd)
 * Fixes / improvements regarding pT-scaling (177eb60)
 * Re-adding ROCcurves (dc90921)

# v2.0.2
 * Fix scaling for injection (6c7f3a3)
 * Adding Pipeline for injection (dc49792)
 * Add error message if tree names are not unique in injection (2f32515) 
 * Improve speed of pT-based scaling (a3494fd)

# v2.0.1
 * Injecting -1 in case values don't exist, thereby avoiding unintential code crashes (8851d8a8)

# v2.0
 * Upgrade of the code to version 2.0.
 * Large overhaul of several pieces and structures.
 * Changing from Keras/Tensorflow to Pytorch.
 * For more details see the merge commit associated to this tag

# v1.10.0
 * fixing an issue with optimisation script producing wrong configs (030dc6e)
 * Regression fixes (ba18cb3)
 * Dont predict if xin is empty (b0e81ce)
 * Fixing BDT loss (3d93ee9)
 * GNN quality of life improvements (fd00d56)
 * List of vectors reading fix (a06730a)
 * Fixing model loading and some incorrect strings (d8b150d)
 * Fixing missing string formater (4f75e5f)
 * Fixing an issue where e_number % nFolds is float instead of int (ceb907c)
 * Fixing an issue when reading vectors containing ',' (60bd7a2)
 * Fixing an issue with ModelBinning not being rendered properly (f03ab9b)
 * Allow strings for TrainingLabel for classification (cf189db)
 * Implementing treatment of loss for BCE and CCE, which need to be slightly different (7abae2c)
 * Adding additional graph checks (bd96236)
 * Removing print statement in conversion handler (e4f2bcf)
 * Adding softmax as possible function (4dc7a5e)
 * Updating hetero gnn (4dc7a5e)
 * fixing conflicts with master (1a764c5)

# v1.9.0
 * fixing matrix missmatch in homogeneous GNN (5e352bd)
 * also sending testing data to 'device' thereby fixing an issue when running with cuda (0a4d2c5)
 * Adding OutputSize to GNN settings documentation (a045dc2)
 * Fixing heterogeneous node types fixed to 2 (44e546f)
 * removing print statement (6e83c05)
 * Fixing an issue with NONE in early stopping (58d3d23)

# v1.8.0
 * Updating documentation (2ca124f),(3fca65f)
 * Adding some documentation on BDTs (76d42a7)
 * Fixing image paths in documentation (8bf437d)
 * Implementation of GNNs (361c51d),(bf0ef66) 
 * Adding plots of scaled variables in case 'InputScaling' is used (7b07fd5)
 * Adding 'LossPlotXScale' and 'LossPlotYScale' as config options (5faaf54)
 * Removing print statement in conversionhandler (73f9d20)
 * Fix wrong factor in BDT loss (acd547d)
 * Adding more questions to the FAQ section (aa7d499)

# v1.7.0
 * Ignoring CI directories in gitignore (3b7c515)
 * Adding FAQ (9d75082)
 * Fixing issues with mkdocs file (ca9bb2e)
 * Updating mkdocs file (f214f10)
 * Adding new markdown extensions to support FAQ (9ae8946)
 * Catching wrong 'MaxFeatures' settings when HO is run (66107e6)
 * Updating HO summary to also work for BDTs (e05e1d9)
 * Enabling `SummariseOptimisation` to also work for BDTs; slightly changing how certain HO parameters are displayed in the summary; Fixing an issue with HO not creating config files correctly (918d98f)
 * Fixing an issue with sklearn not transposing binary output properly (4b8285b)

# v1.6.2
 * Adding check for sklearn to determine whether 'deviance' or 'log_loss' is supported (c8889ff)
 * Document in the Short_walkthrough that one can use composite variables in the Conversion step (7a4e2b8)

# v1.6.1
 * Adding a set check for TrainLabel and FillColor and implementing exception cases for checks(f66045d)

# v1.6.0
 * Fixing wrong injection of 1-p instead of p for binary BDT models (531b812)
 * Updating usage of sklearn functions in BDTModel due to deprecation warnings (9a50418)
 * Adding aliases to make executing the code easier; also correcting some typos (02d4da1)
 * Adding information on aliases to README (7313357)
 * Updating available BDT losses (15af847)
 * Updating setup script (c2a003b)
 * Bdt deprecation warnings and loss updates (9e0db56)
 * Outsourcing HO and implementing it also for BDTs (b04632f)
 * Update conversion documentation to make the changes from c++ to python logic strings clearer (4d97186)
 * Settings 'Folds' default to 2 and implementing check for it (Settings 'Folds' default to 2 and implementing check for it (59b9cd4)
 * Fixing an issue where option checks where not properly run in all cases (be5235a)
 * Updating checks in setup script for tensorflow and numpy (73d3461)
 * Updating information on LCGstacks and adding LCG_102cuda (cad096d)
 * Fixing an issue with deprecation warnings related to keras weighted metrics in compile method (02516b5)
 * Fixing an issue in the conversion handler related to identification of c++ characters (f4a2514)

# v1.5.0
 * Implementing loss function plots for BDTs (e830f6e)

# v1.4.0
 * Add 2D plots for 4 or more classes (66238e2)
 * Improving output of setup script and HTCondorMain script (5085057)
 * Updating LCG Stack for HTCondor and forcing verbosity to level 2 for DNN optimisation to cut down on the size of output files being created. (7f764e1)

# v1.3.1
 * Fixing an issue with 2D plots for 3class NN which lead to wrong 2D stack plots (971c941)
 * Fixing an issue in SummariseOptimisation script by updating getter functions in SummariseOptimisation script (f07f32e)

# v1.3.0
 * Implementing an option so the user can define the 'NtuplePath' in the config file (1ea6567)
 * Adding treatment of cases where no GENERAL selection was given, leading to an error (702f0b6)
 * Adding the (internal) ability to define specific cases for internal option checks in the future(ad7f1c7)

# v1.2.0
 * Implementation of multi regression and expanding generation of CI ntuples to include more realistic truth variables (37d81a3)
 * Adding check for Variable blocks with identical 'Name' parameters (7035a9a)
 * Adding some more information on a few LCG stacks (dd3c30e)

# v1.1.4
 * Add LICENSE (d64b358)
 * Adding more information on LCGStacks and replacing buggy default LCG stack suggestion to the web documentation (0a02e72)
 * Updating web documentation w.r.t. the usage of Keras, Tensorflow and scikit-learn (7f3ef0e)

# v1.1.3
 * Fixing an issue with ROC curve labels. X and Y axis were swapped. (c5de1ad)

# v1.1.2
 * Adding if statement to clean up to prevent crash in evaluation script if no data is defined (9e72c12)
 * Fixing an issue with if statements in HTCondorMain leading to broken jobs (3f46391)

# v1.1.1
 * Fixing an issue which did not allow the user to use != for MCWeights (68bc4ee)
 * Adding defaults for SetActivationFunctions to fix an issue in the optimise script (a020aaa)

# v1.1.0
 * Adding an if statements to check whether the given training labels start with 0 and are consecutive (e7122f3)
 * Adding 'PermImportanceShuffles' to give the user the option to set the number of shuffles (244d91b)
 * Setting stackplotscale back to linear in binary CI (5f44d26)
 * Implementing 'SBBlinding' and 'SignificanceBlinding' to have more options to blind data in plots (3f6f01e)
 * Fixing errorband in lower pad and moving lines for scale, ymax and ymin into right function (9da901a)
 * Setting default for Ymin to 0.0 (2788c5c)
 * removing file which should not be part of the repo (3bffb41)
 * Implementing 'Ymax', 'Ymin' and 'Scale' option for Ctrl plots (b501bfa)
 * Adding an error message in case the scaler is not recalculated but the variables are changed in the config file (fd337b5)
 * Adding command line option '--plots' for evaluation step and classification models. (ef1ee96)
 * Yield check implementation in CI (26b151d)
 * Bdt updates (5c03dfa)
 * Adding a check which prevents the usage of old classification files. (492182c)
 * Adding if statement to treat case where no scaler is used (248ffed)
 * Handling of uncertainties and confusion matrix fix (78bb5c0)
 * Fixing the wrong handling of uncertainties in data/MC plots. Now the MC errorband as well as the data errors should be correctly scaled. (9ca4e37)
 * Implementing 'ConfusionMatrixNorm' option to circumvent an issue where the default sklearn implementation did not accept the 'normalize' parameter. This allows the user to choose between row-wise, column-wise or no normalisation. (a1fff33)
 * Fixing an issue which did accidentally not allow for != operators to be used in selection strings (fc7cb27)
 * Documentation Updates (f19efbb)
 * Removing function corresponding to checking of groups and trainlabel (4012345)
 * changing python requirement from 3.8 back to 3.6 (b055bbc)
 * Adding a function which proposes similar options in case an unallowed option is used (8987dba)
 * Fixing an issue that occured when groups where declared in the config file(396aaca)
 * Fixing wrong handling of uncertainties in ratio pad (20df469)
 * CI update (1ca6ed0)
 * Adding option to set scale for permutation importance plots (bb8205f)
 * Adding check to make sure ROCSampling is smaller than available fpr values (6cfa7ee)
 * Making the CI ntuples a bit more realistic (ce07f79)
 * Fixing Errorpropagation for OptionHandler (ba3a1e9)
 * Improving weight comparison plots (75b098a)
 * Fixing an issue where ClassLabels was not correctly marked as deprecated (d43bace)
 * Adding regression to CI (b6bfce8)
 * initial commit of CI regression(ff778dc)
 * Fixing wrong naming of member variables for labels plotting (7268da2)

# v1.0.2
 * Improving style of code (fa47afa)

# v1.0.1
 * Removing remove emacs, nano and numpy installation to slim docker image. (d777d54)
